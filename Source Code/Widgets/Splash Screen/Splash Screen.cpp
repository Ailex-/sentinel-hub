#include "Splash Screen.h"

SplashScreen::SplashScreen(){

	this->setAttribute(Qt::WA_TranslucentBackground);
	this->setWindowFlag(Qt::FramelessWindowHint);

	QLabel * Image = new QLabel();
	Image->setPixmap(QPixmap(":Splash"));

	QVBoxLayout * MainLayout = new QVBoxLayout(this);
		MainLayout->addWidget(Image,0,Qt::AlignCenter);


	this->setFixedSize(Image->size());
	this->setLayout(MainLayout);
	this->setWindowOpacity(0);
	this->show();


	//----- Animation stuff --

	QPropertyAnimation* FadeIn = new QPropertyAnimation(this, "windowOpacity",this);
		FadeIn->setStartValue(0.0);
		FadeIn->setEndValue(1.0);
		FadeIn->setDuration(1000);


	QPropertyAnimation * FadeOut = new QPropertyAnimation(this, "windowOpacity",this);
		FadeOut->setDuration(1000);
		FadeOut->setStartValue(1);
		FadeOut->setEndValue(0);

	QSequentialAnimationGroup * group = new QSequentialAnimationGroup(this);
		group->addAnimation(FadeIn);
		group->addPause(800);
		group->addAnimation(FadeOut);
		group->start();

	QObject::connect(FadeOut, SIGNAL(finished()), this, SLOT(Close()));
}