#ifndef SENTINEL_HUB_SPLASH_SCREEN_H
#define SENTINEL_HUB_SPLASH_SCREEN_H

#include <QWidget>
#include <QLabel>
#include <QLayout>
#include <QPixmap>
#include <QPropertyAnimation>
#include <QSequentialAnimationGroup>

#include "../../Functions/ParseStyleSheet.h"
#include "../../Functions/Macros/FadeIn.h"

#include "../Login Screen/Login Screen.h"

class SplashScreen : public QWidget {

	Q_OBJECT;

	public:
		SplashScreen();

	private slots:

		void Close() {
			this->deleteLater();
			new LoginPage;
		}

};


#endif