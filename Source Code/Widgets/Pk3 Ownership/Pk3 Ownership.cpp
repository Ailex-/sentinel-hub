#include "Pk3 Ownership.h"

Pk3Ownership::Pk3Ownership(QString AccountName) {
	
	// Geometry & animation
	this->setFixedSize(400, 600);
	this->FadeIn();

	this->Name = AccountName;
	this->AccountName->setText("Account : "+ this->Name);
	this->Pk3List->setFixedSize(280, 365);

	// Fonts
	this->AccountName->setFont(QFont("Dumbledor 3", 12));
	this->Title->setFont(QFont("BlackChancery", 16));
	this->Status->setFont(QFont("Dumbledor 3", 12));
	this->Pk3List->setFont(QFont("Dumbledor 3", 14));

	// Layouts & style

	QHBoxLayout * BottomLayout = new QHBoxLayout;
		BottomLayout->addSpacing(75);
		BottomLayout->addWidget(Status);
		BottomLayout->addStretch();
		BottomLayout->addWidget(OwnershipButton);
		BottomLayout->addSpacing(65);

	QVBoxLayout * MainLayout = new QVBoxLayout;
		MainLayout->addSpacing(40);
		MainLayout->addLayout(HCentered(this->AccountName));
		MainLayout->addSpacing(20);
		MainLayout->addLayout(HCentered(this->Title));
		MainLayout->addSpacing(-25);
		MainLayout->addLayout(HCentered(this->Pk3List));
		MainLayout->addLayout(BottomLayout);
		MainLayout->addSpacing(30);
		MainLayout->addLayout(HCentered(this->ExitButton));
		MainLayout->addStretch();

	WRAPPER_LAYOUT(MainLayout);
	OwnershipButton->setObjectName("ButtonSmall");
	ExitButton->setObjectName("ButtonSmallExit");

	this->setStyleSheet(Style(":Pk3Ownership") + Style(":Button-Small"));


	// Button Logic

	connect( Pk3List		,  SIGNAL( itemClicked(QListWidgetItem *)	),  this,  SLOT( UpdateStatus()		));
	connect( OwnershipButton,  SIGNAL( clicked()						),  this,  SLOT( ChangeOwnership()	));
	connect( ExitButton     ,  SIGNAL( clicked()						),  this,  SLOT( FadeOut()			));

	PopulatePk3();

	this->show();
}