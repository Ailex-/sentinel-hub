#ifndef SENTINEL_HUB_PK3_OWNERSHIP_H
#define SENTINEL_HUB_PK3_OWNERSHIP_H

#include <QWidget>
#include <QPushButton>
#include <QListWidget>
#include <QLabel>

#include "../../Ssh/ExecuteCommandSSH.h"

#include "../../Functions/Macros/DraggableWindow.h"
#include "../../Functions/Macros/WrapperLayout.h"

#include "../../Functions/ParseStyleSheet.h"
#include "../../Functions/CenteredLayout.h"

class Pk3Ownership : public QWidget {
	Q_OBJECT
	DRAGGABLE_WINDOW
	public:
		Pk3Ownership(QString AccountName);

	private:
		QPushButton * ExitButton      = new QPushButton("Close"  , this);
		QPushButton * OwnershipButton = new QPushButton("Change" , this);

		QListWidget * Pk3List		  = new QListWidget(this);
		
		QLabel * AccountName = new QLabel("Account : "   , this);
		QLabel * Status      = new QLabel("Status : "    , this);
		QLabel * Title       = new QLabel("Select a Pk3" , this);

		QString Name;

		void PopulatePk3() {
			QString Output = QString::fromStdString(CommandSSH("ls Pk3"));
			Output = Output.replace("_", " ");

			QStringList Buffer = Output.split("\n");
			Buffer.pop_back();

			QPixmap Empty(30, 30);
			Empty.fill(Qt::transparent);

			for (int i = 0; i < Buffer.size(); i++) {
					Pk3List->addItem(new QListWidgetItem(QIcon(Empty), Buffer[i]));
				}

			int count = Pk3List->count();
			for (int i = 0; i < count; i++)
				Pk3List->item(i)->setSizeHint(QSize(1, 40));

		}

	private slots:
		void FadeIn()  { FADE_IN_ANIMATION;  }
		void FadeOut() { FADE_OUT_ANIMATION; }

		void ChangeOwnership() {
			// Get Owners from server and format output
			std::string Path = "Pk3/" + Pk3List->currentItem()->text().replace(' ', '_').toStdString();  // helper to not write it a lot of times
			QStringList Owners = QString::fromStdString(CommandSSH("cat "+ Path + "/Owner")).split('\n');
			Owners.removeAll("");
			
			bool Found = false;

			for (int i = 0; i < Owners.size(); i++) {

				// If there is the current account in the owners, we have to remove it
				if (Owners[i] == Name.toLower()) {
					Owners.removeAt(i);
					Found = true;
					break;

				}
			}
			// Add the account name if it wasn't present, then write to file
				if (Found == false)
					Owners.append(Name.toLower());

				Path = Path + "/Owner";
				std::string Msg = "rm " + Path + " && touch " + Path;

				for (int i = 0; i < Owners.size(); i++) {
					Msg = Msg + " && echo " + Owners[i].toStdString() + " >> " + Path;
				}

				CommandSSH(Msg);
				UpdateStatus();
		}

		void UpdateStatus() {
			// Borrows a lot of code from the `ChangeOwnership()` function
			std::string Path = "Pk3/" + Pk3List->currentItem()->text().replace(' ', '_').toStdString();  // helper to not write it a lot of times
			QStringList Owners = QString::fromStdString(CommandSSH("cat " + Path + "/Owner")).split('\n');
			Owners.removeAll("");

			bool Found = false;

			for (int i = 0; i < Owners.size(); i++) {
				if (Owners[i] == Name.toLower()) {
					Status->setText("Status : Owner");
					return;
				}
			}
			Status->setText("Status : Not owner");
		}
};

#endif // !SENTINEL_HUB_PK3_OWNERSHIP_H
