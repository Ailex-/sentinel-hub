#include "AccountPage.h"

AccountPage::AccountPage(QWidget* Link) {

	this->setFixedSize(1100, 650);
	this->Link = Link;
	this->FadeIn();


	Accounts->setFixedSize(220,400);
	MainWidget->setFixedSize(670,420);

	QLabel * AccountLabel = new QLabel(this);
	AccountLabel->setText("Accounts");
	AccountLabel->setFont(QFont("BlackChancery", 20));
	AccountLabel->setAlignment(Qt::AlignHCenter);



	QVBoxLayout * AccountLayout = new QVBoxLayout;
		AccountLayout->addSpacing(71);
		AccountLayout->addWidget(AccountLabel);
		AccountLayout->addSpacing(4);
		AccountLayout->addWidget(Accounts);
		AccountLayout->addStretch();
		

	QVBoxLayout * BackLayout = new QVBoxLayout();
		BackLayout->addSpacing(33);
		BackLayout->addWidget(BackButton);
			BackButton->setObjectName("BackButton");
			BackButton->setFixedSize(60,20);
		BackLayout->addStretch();

	
	QVBoxLayout * MainWidgetLayout = new QVBoxLayout;
		MainWidgetLayout->addSpacing(40);
		MainWidgetLayout->addWidget(MainWidget);
		MainWidgetLayout->addStretch();

	QHBoxLayout * MainLayout = new QHBoxLayout(this);
		MainLayout->addSpacing(62);
		MainLayout->addLayout(AccountLayout);
		MainLayout->addSpacing(35);
		MainLayout->addLayout(MainWidgetLayout);
		MainLayout->addSpacing(-24);
		MainLayout->addLayout(BackLayout);
		MainLayout->addStretch();


	WRAPPER_LAYOUT(MainLayout);

	CreateLayout_MainWidget();

	this->setStyleSheet(Style(":AccountPage") + Style(":Button-Small"));
	this->show();


	connect(ChangePasswordBtn, SIGNAL(clicked()), this, SLOT(ChangePassword()));
	connect(ShowPassword, SIGNAL(stateChanged(int)), this, SLOT(PasswordEcho()));
	connect(BackButton, SIGNAL(clicked()), this, SLOT(GoBack()));

	connect( GroupAdmin		, SIGNAL( clicked() ),  this,  SLOT( SlotAdmin()	 ));
	connect( GroupPk3		, SIGNAL( clicked() ),  this,  SLOT( SlotPk3()		 ));
	connect( GroupDeveloper	, SIGNAL( clicked() ),  this,  SLOT( SlotDeveloper() ));
	connect( GroupRcon		, SIGNAL( clicked() ),  this,  SLOT( SlotRcon()		 ));
	connect( Pk3Owner		, SIGNAL( clicked() ),  this,  SLOT( SlotPk3Owner()	 ));
}