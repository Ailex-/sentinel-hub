#ifndef SENTINEL_ACCOUNT_PAGE_H
#define SENTINEL_ACCOUNT_PAGE_H

#include <QWidget>
#include <QLabel>
#include <QLayout>
#include <QPushButton>
#include <QListWidget>
#include <QPropertyAnimation>
#include <QCheckBox>
#include <QThread>
#include <QLineEdit>

#include "../../Ssh/ExecuteCommandSSH.h"

#include "../../Functions/Macros/DraggableWindow.h"
#include "../../Functions/Macros/WrapperLayout.h"

#include "../../Functions/ParseStyleSheet.h"
#include "../../Functions/CenteredLayout.h"

#include "../Pk3 Ownership/Pk3 Ownership.h"

extern QString Pass;

class AccountPage : public QWidget {

	
	Q_OBJECT
	DRAGGABLE_WINDOW

	public :
		AccountPage(QWidget * Link);

	private :
		QWidget * Link;
		
		QWidget * MainWidget = new QWidget(this);
		QListWidget * Accounts = new QListWidget(this);


		QStringList UserInfo;

		QPushButton * ChangePasswordBtn = new QPushButton(this);
		QPushButton * DeleteUser     = new QPushButton(this);
		QPushButton * Pk3Owner       = new QPushButton(this);
		QPushButton * BackButton     = new QPushButton(this);


		QCheckBox * GroupAdmin       = new QCheckBox(this);
		QCheckBox * GroupRcon        = new QCheckBox(this);
		QCheckBox * GroupDeveloper   = new QCheckBox(this);
		QCheckBox * GroupPk3         = new QCheckBox(this);
		
		QLineEdit * Password1 = new QLineEdit(this);
		QLineEdit * Password2 = new QLineEdit(this);

		QCheckBox * ShowPassword     = new QCheckBox(this);


		void CreateLayout_MainWidget();

		
	public slots:
		void FadeIn() { 	
			QPropertyAnimation * FadeIn = new QPropertyAnimation(this, "windowOpacity");
			FadeIn->setStartValue(0.0);
			FadeIn->setEndValue(1.0);
			FadeIn->setDuration(500);
			FadeIn->start(QAbstractAnimation::DeleteWhenStopped);
			connect(FadeIn, SIGNAL(finished()), this, SLOT(show()));
			move(
				this->screen()->size().width() / 2 - this->width() / 2,
				this->screen()->size().height() / 2 - this->height() / 2
			);
		}
		
	private slots:
		void FadeOut() {
			QPropertyAnimation* FadeOut = new QPropertyAnimation(this, "windowOpacity");
			FadeOut->setDuration(200);
			FadeOut->setStartValue(1);
			FadeOut->setEndValue(0);
			FadeOut->start();
			connect(FadeOut, SIGNAL(finished()), Link, SLOT(FadeIn()));
			connect(FadeOut, SIGNAL(finished()), this, SLOT(close()));
		}


		void ChangePassword() {

			if (Password1->text().isEmpty() || Password2->text().isEmpty()) {
				new Error(600);
				return;
			}
			if (Password1->text() != Password2->text()) {
				new Error(601);
				return;
			}

			CommandSSH(QString("echo -e \""+Pass+"\n"+Password1->text()+"\n"+Password1->text()+"\" | passwd hub-"+Accounts->currentItem()->text().toLower()).toStdString());

		}

		void PasswordEcho(){
			if (ShowPassword->isChecked()) {
				Password1->setEchoMode(QLineEdit::Normal);
				Password2->setEchoMode(QLineEdit::Normal);
			} else {
				Password1->setEchoMode(QLineEdit::Password);
				Password2->setEchoMode(QLineEdit::Password);
			}
		}


		void GoBack() {
			Link->show();
			this->FadeOut();
		}

		void SlotAdmin() {
			if (GroupAdmin->isChecked())
				qDebug() << "Checked";
			else
				qDebug() << "No Check";
		}
		void SlotPk3() {
			if (GroupPk3->isChecked())
				qDebug() << "Checked";
			else
				qDebug() << "No Check";
		}
		void SlotRcon() {
			if (GroupRcon->isChecked())
				qDebug() << "Checked";
			else
				qDebug() << "No Check";
		}
		void SlotDeveloper() {
			if (GroupDeveloper->isChecked())
				qDebug() << "Checked";
			else
				qDebug() << "No Check";
		}

		void SlotPk3Owner() {
			new Pk3Ownership(Accounts->currentItem()->text());
		}
};

#endif