#include "AccountPage.h"

void AccountPage::CreateLayout_MainWidget() {

	QLabel * LabelLeft = new QLabel(this);
	LabelLeft->setText("Permissions");
	LabelLeft->setFont(QFont("BlackChancery", 16));
	LabelLeft->setAlignment(Qt::AlignHCenter);


	QLabel * LabelRight = new QLabel(this);
	LabelRight->setText("Change Password");
	LabelRight->setFont(QFont("BlackChancery", 16));
	LabelRight->setAlignment(Qt::AlignHCenter);


	GroupDeveloper->setText("Developer");
	GroupDeveloper->setFont(QFont("Dumbledor 3", 14));

	GroupAdmin->setText("Account Management");
	GroupAdmin->setFont(QFont("Dumbledor 3", 14));

	GroupPk3->setText("Pk3 Files");
	GroupPk3->setFont(QFont("Dumbledor 3", 14));

	GroupRcon->setText("Server Managment");
	GroupRcon->setFont(QFont("Dumbledor 3", 14));


	Pk3Owner->setText("Pk3 Ownership");
	Pk3Owner->setFont(QFont("Dumbledor 3", 14));
	Pk3Owner->setObjectName("ButtonSmall");
	Pk3Owner->setFixedWidth(240);


	QVBoxLayout* LeftLayout = new QVBoxLayout();
		LeftLayout->addWidget(LabelLeft);
		LeftLayout->addSpacing(15);
		LeftLayout->addWidget(GroupDeveloper);
		LeftLayout->addSpacing(10);
		LeftLayout->addWidget(GroupAdmin);
		LeftLayout->addSpacing(10);
		LeftLayout->addWidget(GroupPk3);
		LeftLayout->addSpacing(10);
		LeftLayout->addWidget(GroupRcon);
		LeftLayout->addSpacing(110);
		LeftLayout->addWidget(Pk3Owner);
		LeftLayout->addStretch();

	// UP   - Left side
	//
	// DOWN - Right side

	Password1->setPlaceholderText("Type new password");
	Password1->setFont(QFont("Dumbledor 3", 13));
	Password1->setEchoMode(QLineEdit::Password);
	Password1->setFixedWidth(320);

	Password2->setPlaceholderText("Retype new password");
	Password2->setFont(QFont("Dumbledor 3", 13));
	Password2->setEchoMode(QLineEdit::Password);
	Password2->setFixedWidth(320);

	ChangePasswordBtn->setText("Submit");
	ChangePasswordBtn->setFont(QFont("Dumbledor 3", 14));
	ChangePasswordBtn->setObjectName("ButtonSmall");
	ChangePasswordBtn->setFixedWidth(150);

	ShowPassword->setText("Show Password");
	ShowPassword->setFont(QFont("Dumbledor 3", 14));

	QHBoxLayout * RightLayout2 = new QHBoxLayout;
		RightLayout2->addSpacing(10);
		RightLayout2->addWidget(ShowPassword);
		RightLayout2->addSpacing(10);
		RightLayout2->addWidget(ChangePasswordBtn);
		RightLayout2->addSpacing(10);


	DeleteUser->setText("Delete User");
	DeleteUser->setFont(QFont("Dumbledor 3", 14));
	DeleteUser->setObjectName("ButtonSmallExit");
	DeleteUser->setFixedWidth(150);

	QVBoxLayout* RightLayout = new QVBoxLayout();
		RightLayout->addWidget(LabelRight);
		RightLayout->addSpacing(15);
		RightLayout->addWidget(Password1);
		RightLayout->addSpacing(10);
		RightLayout->addWidget(Password2);
		RightLayout->addSpacing(10);
		RightLayout->addLayout(RightLayout2);
		RightLayout->addSpacing(125);
		RightLayout->addWidget(DeleteUser);
		RightLayout->addStretch();

	QHBoxLayout * SplitLayout = new QHBoxLayout();
		SplitLayout->addLayout(LeftLayout);
		SplitLayout->addLayout(RightLayout);


	QVBoxLayout * Y = new QVBoxLayout();
		Y->addSpacing(75);
		Y->addLayout(SplitLayout);
		Y->addStretch();

		MainWidget->setLayout(Y);


		Accounts->setFont(QFont("Dumbledor 3", 16));
		Accounts->setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);

		// Get Accounts

		QStringList buf = QString::fromStdString(CommandSSH("cut -d: -f1 /etc/passwd | grep \"hub-\"")).split("\n");
		QString Name;

		for (int i = 0; i < buf.length()-1; i++) {
			Name = buf[i].remove("hub-");
			Name[0] = Name[0].toUpper();

			Accounts->addItem(new QListWidgetItem(Name));
			Accounts->item(i)->setSizeHint(QSize(1, 40));
		}
		Accounts->setCurrentRow(0);

		// Check groups

		buf = QString::fromStdString(CommandSSH("groups")).remove('\n').split(' ');
		for (int i = 0; i < buf.length(); i++) {
			if ( buf[i] == "hub-user"	) { GroupPk3       -> setChecked(true); continue; }
			if ( buf[i] == "hub-dev"	) { GroupDeveloper -> setChecked(true); continue; }
			if ( buf[i] == "hub-admin"	) { GroupAdmin	   -> setChecked(true); continue; }
			if ( buf[i] == "hub-server"	) { GroupRcon	   -> setChecked(true); continue; }
		}
}