#ifndef SENTINEL_HUB_WELCOME_SCREEN_H
#define SENTINEL_HUB_WELCOME_SCREEN_H

#include <QWidget>
#include <QLayout>
#include <QLabel>
#include <QPushButton>
#include <QPropertyAnimation>

#include "../../Functions/Macros/DraggableWindow.h"
#include "../../Functions/Macros/FadeIn.h"
#include "../../Functions/Macros/WrapperLayout.h"

#include "../../Functions/CenteredLayout.h"
#include "../../Functions/ParseStyleSheet.h"

#include "../Select Path/Select Path.h"
#include "../Menu Page/Menu Page.h"


class WelcomePage : public QWidget {
	
	Q_OBJECT
	DRAGGABLE_WINDOW

	public:
		WelcomePage();

	private:
		QPushButton * OkButton = new QPushButton("Ok",this);

	private slots:
		
		void FadeIn()  { FADE_IN_ANIMATION;  }
		void FadeOut() { new SelectPath(new MenuPage(true)); FADE_OUT_ANIMATION; }
};


#endif