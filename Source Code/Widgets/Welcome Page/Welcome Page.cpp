#include "Welcome Page.h"

WelcomePage::WelcomePage() {

	this->setFixedSize(500, 600);

	OkButton->setObjectName("ButtonSmall");
	OkButton->setFont(QFont("Dumbledor 3", 12));
	
	QLabel * Welcome = new QLabel("Welcome",this);
		Welcome->setFont(QFont("Dumbledor 3", 30));

	QLabel * Description = new QLabel(this);
		Description->setFont(QFont("Dumbledor 3", 14)); Description->setWordWrap(true);
		Description->setFixedWidth(400);

	Description->setText("The pk3 manager needs to know where you have installed Movie Battles 2, otherwise the program doesn't"
		" know where to install the various pk3 you select."
		"<br><br> You will get a small prompt on which you can select the GameData folder, you have to do this only for the first time"
		" the program opens or if somemthing breaks, you are also free to change the GameData directory in the future.");


	QVBoxLayout * MainLayout = new QVBoxLayout(this);
		MainLayout->addSpacing(45);
		MainLayout->addLayout(HCentered(Welcome));
		MainLayout->addSpacing(35);
		MainLayout->addLayout(HCentered(Description));
		MainLayout->addStretch();
		MainLayout->addLayout(HCentered(OkButton));
		MainLayout->addSpacing(20);

	WRAPPER_LAYOUT(MainLayout);

	this->setStyleSheet(Style(":WelcomePage") + Style(":Button-Small"));

	this->FadeIn();
	this->show();


	connect(OkButton, SIGNAL(clicked()	), this			, SLOT(FadeOut()	));
}