#include "../Pk3 Page.h"

void Pk3Page::CreateRightPanel() {
	
	RightPanel->setFixedSize(341, 400);
	RightPanel->setObjectName("RightPanel");

	ToggleButton->setFixedWidth(140);

	UploadButton   -> setObjectName("Upload"  );  UploadButton  -> setFont(QFont("Dumbledor 3", 14));
	DownloadButton -> setObjectName("Download");  DownloadButton-> setFont(QFont("Dumbledor 3", 14));
	ToggleButton   -> setObjectName("Toggle"  );  ToggleButton  -> setFont(QFont("Dumbledor 3", 14));
	RemoveButton   -> setObjectName("Remove"  );  RemoveButton  -> setFont(QFont("Dumbledor 3", 14));

	Title -> setStyleSheet("color:gold;");
	Title -> setFont(QFont("BlackChancery", 20));

	Description -> setStyleSheet("color:#e0c122; padding:8px;");
	Description -> setFont(QFont("Dumbledor 3", 14));
	Description -> setWordWrap(true);


	QVBoxLayout * RightPanelLayout = new QVBoxLayout;
		
		
		RightPanelLayout->addLayout(HCentered(Title));
		RightPanelLayout->addSpacing(10);
		RightPanelLayout->addLayout(HCentered(Description));
		RightPanelLayout->addSpacing(25);
		RightPanelLayout->addLayout(HCentered(LogoSpin));
		RightPanelLayout->addStretch();

		RightPanelLayout->addWidget(UploadButton);		RightPanelLayout->addSpacing(3);
		RightPanelLayout->addWidget(DownloadButton);	RightPanelLayout->addSpacing(3);
	
		RightPanelLayout->addLayout(HCentered(ToggleButton, RemoveButton));
	
		RightPanelLayout->addSpacing(20);

	RightPanel->setLayout(RightPanelLayout);


}