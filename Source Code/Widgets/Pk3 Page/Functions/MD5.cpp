#include "../Pk3 Page.h"

std::string Pk3Page::GetMD5(std::string Path) {
	// Returns empty string on error, otherwise it returns the md5 of the file 

	FILE* inFile = fopen(Path.c_str(), "rb");


	unsigned char hash[MD5_DIGEST_LENGTH];
	unsigned char data[1024];

	char md5[MD5_DIGEST_LENGTH*2 + 1]; //== To account to '\0' at the end

	MD5_CTX mdContext;
	size_t bytes;


	MD5_Init(&mdContext);
	while ((bytes = fread(data, 1, 1024, inFile)) != 0)
		MD5_Update(&mdContext, data, bytes);
	MD5_Final(hash, &mdContext);

	fclose(inFile);
	
	for (int i = 0; i < MD5_DIGEST_LENGTH; i++) sprintf(md5+2*i,"%02x", hash[i]);
	


	return md5;

}