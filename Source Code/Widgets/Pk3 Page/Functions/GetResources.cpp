#include "../Pk3 Page.h"

void Pk3Page::GetResources() {

	QString Output = QString::fromStdString(CommandSSH("ls Pk3"));
	Output = Output.replace("_", " ");

	QStringList Buffer = Output.split("\n");
	Buffer.pop_back();


	for (int i = 0; i < Buffer.size(); i++) {

		if (QFile::exists(GamePath + Prefix + Buffer[i] + ".pk3")) {
			Pk3List->addItem(new QListWidgetItem(QIcon(":Downloaded"), Buffer[i]));
			Title->setText(Pk3List->item(i)->text());
			if(LocalPk3Version(0, Title->text().toStdString()) < RemotePk3Version(Title->text()))
				Pk3List->item(i)->setIcon(QIcon(":Update"));

		}
		else if (QFile::exists(GamePath + Prefix + Buffer[i] + ".pk3.disabled")) {
			Pk3List->addItem(new QListWidgetItem(QIcon(":Disabled"), Buffer[i]));
			Title->setText(Pk3List->item(i)->text());
			if (LocalPk3Version(0, Title->text().toStdString()) < RemotePk3Version(Title->text()))
				Pk3List->item(i)->setIcon(QIcon(":Update"));
		}

		else {
			QPixmap Empty(30, 30);
			Empty.fill(Qt::transparent);

			Pk3List->addItem(new QListWidgetItem(QIcon(Empty), Buffer[i]));
		}
	}


	int count = Pk3List->count();
	for (int i = 0; i < count; i++)
		Pk3List->item(i)->setSizeHint(QSize(1,40));
	}