#include "../Pk3 Page.h"


void Pk3Page::SendFile() {

	int mode = 1;

	std::string suffix;
	if (QFile::exists(GamePath + Prefix + Title->text() + ".pk3.disabled"))
		suffix = ".pk3.disabled";
	else { suffix = ".pk3"; }


	std::string Path = GamePath.toStdString() + Prefix.toStdString() + Title->text().toStdString() + suffix;

	Bar->show();
	ThreadPerc->show();
	ThreadSize->show();
	ThreadSpeed->show();
	ThreadPause->show();


	Bar->setValue(0);
	ThreadPerc->setText("0 %");

	Pk3Size   = 0 ;
	RemainingBytes = 0 ;

	//====== If the download button is hidden it means we're uploading ==|
	int flag = 0;
	if (DownloadButton->isHidden()) {
		flag = 1;

	}
	if (flag == 1) {
		// Make sure that we're not uploading the same exact file
		if (GetMD5(GamePath.toStdString() + Prefix.toStdString() + Title->text().toStdString() + suffix) ==
			CommandSSH("md5sum Pk3/" + Title->text().replace(" ", "_").toStdString() + "/Asset.pk3 | head -c32")) {
			new Error(500, Title->text());
			return;
		}
		else {
			// Make sure that we increment the version number
			if (ToggleButton->text() == "Disable") { mode = 0; }

			int Local = LocalPk3Version(mode, Title->text().toStdString());
			if (Local < 0) {
				new Error(Local * -1);
				return;
			}
			else {
				if (RemotePk3Version(Title->text()) <= Local) {
					int rc = IncrementLocalPk3(mode, Title->text().toStdString());
					if (rc != 0) {
						new Error(rc);
						return;
					}
				}
			}
		}
	}


	size_t math;
	Thread = new FileSender(flag, Path, "Pk3/" + Title->text().replace(" ", "_").toStdString() + "/Asset.pk3", &Pk3Size, &RemainingBytes, &Speed);
	Thread->start(QThread::LowestPriority);
	while (!Thread->isFinished()) {

		QApplication::processEvents();


		// -- Math used to find % of downloaded file

		if (Pk3Size == 0) { Bar->setValue(0); }		// Cannot divide by zero
		else {
			math = RemainingBytes;
			math = math * 100;
			math = math / Pk3Size;

			Bar->setValue(static_cast<int>(math));		// It's safe to cast from size_t to int because percentage ranges from 0 to 100
			Bar->repaint();
		}

		ThreadPerc->setText(QString::number(Bar->value()) + " %");
		ThreadSpeed->setText(QString::number(Speed) + " Mb/s");

		ThreadSize->setText(QString::number(RemainingBytes / 1048576) + " Mb / " + QString::number(Pk3Size / 1048576) + " Mb");
		QThread::msleep(5);
	}

	ThreadPerc->setText("100 %");
	Bar->setValue(100);
	Thread = NULL;

	Bar->hide();
	ThreadPerc->hide();
	ThreadSize->hide();
	ThreadSpeed->hide();
	ThreadPause->hide();

	//----- Checksum --|

	if (GetMD5(GamePath.toStdString() + Prefix.toStdString() + Title->text().toStdString() + suffix) !=
		CommandSSH("md5sum Pk3/" + Title->text().replace(" ", "_").toStdString() + "/Asset.pk3 | head -c32")) {
		if (flag == 1) { new Error(400, Title->text()); }
		else { new Error(401, Title->text()); }
	}
	else {
		Pk3List->currentItem()->setIcon(QIcon(":Downloaded"));
		UpdateButtons b(ToggleButton, UploadButton, DownloadButton, RemoveButton, Title);
		b.start();
		while (b.isRunning()) {
			QApplication::processEvents();
			QThread::usleep(1);
		}

	}

}