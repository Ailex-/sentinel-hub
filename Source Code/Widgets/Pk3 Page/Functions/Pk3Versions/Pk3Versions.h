#pragma once
#ifndef SEN_PK3_VERSIONS_H
#define SEN_PK3_VERSIONS_H

#include <QString>
#include <string>
#include <libzippp/libzippp.h>
#include <libssh/libssh.h>

#include "../../../../Ssh/ExecuteCommandSSH.h"
#include "../../../../Errors/Handler.h"

extern ssh_session Session;
extern QString GamePath;
extern QString Prefix;

bool isNumber(const std::string& str);
// 1 = disabled, 0 = enabled
int LocalPk3Version(int mode, std::string Title);

int IncrementLocalPk3(int mode, std::string Title);

int RemotePk3Version(QString Title);

#endif // !SEN_PK3_VERSIONS_H