#include "Pk3Versions.h"

bool isNumber(const std::string& str)
{
	for (int i = 0; i < str.length(); i++) {
		if (std::isdigit(str[i]) == 0) return false;
	}
	return true;
}


// 1 = disabled, 0 = enabled
int LocalPk3Version(int mode, std::string Title) {

	std::string Path = GamePath.toStdString();

	if (mode == 1) {
		Path = Path + Prefix.toStdString() + Title + ".pk3.disabled";
	}
	else {
		Path = Path + Prefix.toStdString() + Title + ".pk3";
	}

	libzippp::ZipArchive zf(Path);
	zf.open(libzippp::ZipArchive::ReadOnly);

	if (zf.hasEntry("Version.txt") == false) {
		new Error(502, QString::fromStdString(Title));
		return -502;
	}

	std::string str2 = zf.getEntry("Version.txt").readAsText();

	zf.close();

	return (std::stoi(str2));
}

int IncrementLocalPk3(int mode, std::string Title) {

	// Even tho it's possible to get the version nummber from LocalPk3Version(),
	//	it's still needed to modify the file value, this is why LocalPk3Version()
	//	is basically re-immplemented here.


	std::string Path = GamePath.toStdString();

	if (mode == 1) {
		Path = Path + Prefix.toStdString() + Title + ".pk3.disabled";
	}
	else {
		Path = Path + Prefix.toStdString() + Title + ".pk3";
	}

	libzippp::ZipArchive zf(Path);
	zf.open(libzippp::ZipArchive::Write);

	if (zf.hasEntry("Version.txt") == false) {
		new Error(502);
		return 502;
	}

	std::string str2 = zf.getEntry("Version.txt").readAsText();

	int version;
	if (isNumber(str2))
		version = std::stoi(str2) + 1;
	else {
		new Error(501);
		return 501;
	}

	//== if not used like this, it wont work for some reason ==|
	std::string x = std::to_string(version);
	const char* msg = x.c_str();

	zf.addData("Version.txt", msg, x.length());

	zf.close();

	return 0;
}


int RemotePk3Version(QString Title) {

	return (std::stoi(CommandSSH("cat Pk3/" + Title.replace(" ", "_").toStdString() + "/Version")));
}
