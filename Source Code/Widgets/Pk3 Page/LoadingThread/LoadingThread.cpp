#include "LoadingThread.h"


LoadingThread::LoadingThread(QListWidget* Pk3List) {
	this -> Pk3List		= Pk3List	  ;
}


void LoadingThread::run() {

	QString Output = QString::fromStdString(CommandSSH("ls Pk3"));
	Output = Output.replace("_", " ");

	QStringList Buffer = Output.split("\n");
	Buffer.pop_back();

	QPixmap Empty(30, 30);
	Empty.fill(Qt::transparent);

	for (int i = 0; i < Buffer.size(); i++) {

		if (QFile::exists(GamePath + Prefix + Buffer[i] + ".pk3")) {

			Pk3List->addItem(new QListWidgetItem(QIcon(":Downloaded"), Buffer[i]));

			if (LocalPk3Version(0, Pk3List->item(i)->text().toStdString()) < RemotePk3Version(Pk3List->item(i)->text()))
				Pk3List->item(i)->setIcon(QIcon(":Update"));

		}
		else if (QFile::exists(GamePath + Prefix + Buffer[i] + ".pk3.disabled")) {

			Pk3List->addItem(new QListWidgetItem(QIcon(":Disabled"), Buffer[i]));

			if (LocalPk3Version(1, Pk3List->item(i)->text().toStdString()) < RemotePk3Version(Pk3List->item(i)->text()))
				Pk3List->item(i)->setIcon(QIcon(":Update"));
		}

		else {

			Pk3List->addItem(new QListWidgetItem(QIcon(Empty), Buffer[i]));
		}
	}

	int count = Pk3List->count();
	for (int i = 0; i < count; i++)
		Pk3List->item(i)->setSizeHint(QSize(1, 40));

	

	this->quit();
}