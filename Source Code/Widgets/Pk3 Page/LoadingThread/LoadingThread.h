#ifndef SEN_THREAD_LOAD_H
#define SEN_THREAD_LOAD_H

/*	Class used to easily Upload or Download a single file to a remote machine usng SFTP		*/
/*	------------------------------------------------------------------------------------	*/

#include <QThread>
#include <QWaitCondition>
#include <QMutex>
#include <QListWidget>

#include <string>



//===== SSH Includes ==
#include <libssh/libssh.h>
#include <libssh/sftp.h>

#include "../../../Ssh/ExecuteCommandSSH.h"
#include "../../../Errors/Handler.h"

#include "../Functions/Pk3Versions/Pk3Versions.h"

extern ssh_session Session;
extern QString GamePath;

class LoadingThread : public QThread {

	public:
		LoadingThread(QListWidget * Pk3List);


		void run();

	private:
		QListWidget * Pk3List;
};

#endif //::SEN_THREAD_LOAD_H::