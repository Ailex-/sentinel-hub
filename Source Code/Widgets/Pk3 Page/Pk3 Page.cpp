#include "Pk3 Page.h"

Pk3Page::Pk3Page(QWidget * Link) {

	this->Link = Link;

	this->setFixedSize(1100, 650);

	LogoSpin->setMovie(Logo);
	LogoSpin->setAlignment(Qt::AlignCenter);
	Description->setAlignment(Qt::AlignCenter);


	Pk3List -> setFixedSize(605, 425);
	Pk3List -> setFont(QFont("Dumbledor 3",16));
	Pk3List -> setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	Pk3List -> setObjectName("test");
	Pk3List -> setSizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
	Pk3List -> setIconSize(QSize(27,27));


	QHBoxLayout * CentralWidget = new QHBoxLayout;
		CentralWidget->addSpacing(60);
		CentralWidget->addWidget(Pk3List);
		CentralWidget->addWidget(RightPanel);
		CentralWidget->addStretch();
		CentralWidget->setSpacing(0);



	Bar->setMaximum(100);
	Bar->setTextVisible(false);
	Bar->setFixedWidth(750);

		QPixmap * mask = new QPixmap(":Mask");
			Bar->setMask(mask->mask());

			ThreadPause->setFixedSize(60, 24);
			ThreadPause->setObjectName("DownloadPause");

		QPixmap * maskPause = new QPixmap(":MaskPause");
			ThreadPause->setMask(maskPause->mask());


	QHBoxLayout * BarLayout = new QHBoxLayout;
		BarLayout->addSpacing(80);
		BarLayout->addWidget(Bar);
		BarLayout->addSpacing(-42);
		BarLayout->addWidget(ThreadPause);
		BarLayout->addSpacing(12);
		BarLayout->addStretch();
		BarLayout->addWidget(ThreadPerc);
		BarLayout->addStretch();
		BarLayout->addSpacing(120);


		ThreadPerc->setFont(QFont("Dumbledor 3", 18));
		ThreadSpeed->setFont(QFont("Dumbledor 3", 12));
		ThreadSize->setFont(QFont("Dumbledor 3", 12));


	QHBoxLayout * IndicatorLayout = new QHBoxLayout;
		IndicatorLayout->addSpacing(680);
		IndicatorLayout->addWidget(ThreadSpeed);
		IndicatorLayout->addStretch();
		IndicatorLayout->addWidget(ThreadSize);
		IndicatorLayout->addSpacing(120);
		

	QHBoxLayout* CloseButtonLayout = new QHBoxLayout;
		CloseButtonLayout->addSpacing(620);
		CloseButtonLayout->addWidget(BackButton);
		CloseButtonLayout->addStretch();


	QVBoxLayout * MainLayout = new QVBoxLayout(this);
		MainLayout->addSpacing(58);
		MainLayout->addLayout(CloseButtonLayout);
		MainLayout->addSpacing(-20);
		MainLayout->addLayout(HCentered(Pk3List, RightPanel));
		MainLayout->addSpacing(-5);
		MainLayout->addLayout(IndicatorLayout);
		MainLayout->addSpacing(20);
		MainLayout->addLayout(BarLayout);
		MainLayout->addStretch();
		MainLayout->setSpacing(0);


	WRAPPER_LAYOUT(MainLayout);

	

	Bar->setMask(QPixmap(":Pk3-Download-Mask").mask());

	ThreadPause->setFixedSize(60, 24);
	ThreadPause->setObjectName("DownloadPause");
	ThreadPause->setMask(QPixmap(":Pk3-Pause-Mask").mask());

	ThreadPause->setIcon(QPixmap(":Resume").scaled(12,12));

	BackButton->setObjectName("BackButton");
	BackButton->setFixedSize(60, 20);
	BackButton->setMask(QPixmap(":BackButton").mask());

	this->setStyleSheet(Style(":Pk3Page"));
	this->show();
	
	connect(Pk3List			, SIGNAL(itemClicked(QListWidgetItem *)	), this	, SLOT(SelectPk3()		));
	connect(RemoveButton	, SIGNAL(clicked()						), this , SLOT(RemovePk3()		));
	connect(ToggleButton	, SIGNAL(clicked()					    ), this	, SLOT(Toggle()			));
	connect(DownloadButton	, SIGNAL(clicked()						), this	, SLOT(SendFile()		));
	connect(UploadButton	, SIGNAL(clicked()						), this	, SLOT(SendFile()		));
	connect(ThreadPause		, SIGNAL(clicked()						), this	, SLOT(PauseDownload()	));
	connect(BackButton      , SIGNAL(clicked()                      ), this , SLOT(GoBack()         ));


	ToggleButton->hide();
	UploadButton->hide();
	DownloadButton->setText("Install");
	DownloadButton->hide();
	RemoveButton->hide();

	Bar->hide();
	ThreadPerc->hide();
	ThreadSize->hide();
	ThreadSpeed->hide();
	ThreadPause->hide();

	BackButton->raise();

	CreateRightPanel();
	FadeIn();

}