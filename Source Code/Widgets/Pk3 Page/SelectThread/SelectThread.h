#ifndef SENTINEL_SELECT_THREAD_H
#define SENTINEL_SELECT_THREAD_H

#include <QThread>
#include <QLabel>
#include <QListWidget>
#include <QFile>

#include "../../../Ssh/ExecuteCommandSSH.h"
#include "../Functions/Pk3Versions/Pk3Versions.h"

class SelectThread : public QThread {

	public:
		SelectThread(QString Title, QString* Output);
		

		void run();

	private:
		QString Title;
		QString * Output;

};

#endif // SENTINEL_SELECT_THREAD_H
