#ifndef RESOURCES_PAGE_H
#define RESOURCES_PAGE_H

#include <QLayout>
#include <QLabel>
#include <QListWidget>
#include <QPropertyAnimation>
#include <QProgressBar>
#include <QPushButton>
#include <QApplication>
#include <QDir>
#include <QFile>
#include <QMovie>

#include "Functions/Pk3Versions/Pk3Versions.h"
#include "LoadingThread/LoadingThread.h"
#include "SelectThread/SelectThread.h"
#include "UpdateButtonsThread/UpdateButtons.h"

#include "../../Functions/Macros/DraggableWindow.h"
#include "../../Functions/Macros/WrapperLayout.h"

#include "../../Functions/ParseStyleSheet.h"
#include "../../Functions/CenteredLayout.h"

#include "../../Ssh/ExecuteCommandSSH.h"
#include "../../Ssh/FileSender/FileSender.h"

#include <openssl/md5.h>


extern QString GamePath;
extern QString User;
extern QString Prefix;

class Pk3Page : public QWidget {

	Q_OBJECT
	DRAGGABLE_WINDOW

	public:
		Pk3Page(QWidget * Link);

	private:

		QWidget * Link;

		QWidget	* RightPanel = new QWidget(this); 
		void CreateRightPanel();

		QListWidget * Pk3List = new QListWidget(this); void GetResources();
		
		QLabel * Title		 = new QLabel("",this);
		QLabel * Description = new QLabel("",this);
		QLabel * LogoSpin    = new QLabel("",this);
		QMovie * Logo		 = new QMovie(":LogoAnimated", QByteArray(), this);

		QProgressBar * Bar = new QProgressBar(this);

		bool Busy = false; // To prevent spamming buttons

		QPushButton * UploadButton	 = new QPushButton("Send Update", this);
		QPushButton * DownloadButton = new QPushButton("Install"    , this);
		QPushButton * ToggleButton   = new QPushButton("Enable"     , this); // Or disable
		QPushButton * RemoveButton   = new QPushButton("Delete"     , this);
		QPushButton * BackButton     = new QPushButton(this);



		std::string GetMD5(std::string Path);

		//------ Thread used for upload and download -----------------|

		FileSender * Thread = NULL;

		QPushButton * ThreadPause  = new QPushButton(this);

		QLabel * ThreadPerc  = new QLabel("", this);
		QLabel * ThreadSpeed = new QLabel("", this);
		QLabel * ThreadSize  = new QLabel("", this);

		size_t RemainingBytes = 0;
		size_t Pk3Size   = 0;
		size_t Speed	 = 0;


	public slots:

		void FadeIn() {
			
			Pk3List->clear();
			LogoSpin->show();
			LogoSpin->movie()->start();

			Title->hide();
			Description->hide();

			ToggleButton->hide();
			UploadButton->hide();
			DownloadButton->hide();
			RemoveButton->hide();

			BackButton->clearFocus();
			BackButton->repaint();

			QPropertyAnimation * FadeIn = new QPropertyAnimation(this, "windowOpacity");
			FadeIn->setStartValue(0.0);
			FadeIn->setEndValue(1.0);
			FadeIn->setDuration(500);
			FadeIn->start(QAbstractAnimation::DeleteWhenStopped);
			connect(FadeIn, SIGNAL(finished()), this, SLOT(PopulatePk3()));

			move(
				this->screen()->size().width() / 2 - this->width() / 2,
				this->screen()->size().height() / 2 - this->height() / 2
			);
		}

		void FadeOut() {
			QPropertyAnimation* FadeOut = new QPropertyAnimation(this, "windowOpacity");
			FadeOut->setDuration(200);
			FadeOut->setStartValue(1);
			FadeOut->setEndValue(0);
			FadeOut->start();
			connect(FadeOut, SIGNAL(finished()), Link, SLOT(FadeIn()));
			connect(FadeOut, SIGNAL(finished()), this, SLOT(close()));
		}


	private slots:
		void PopulatePk3() {

			LoadingThread a(Pk3List);
			a.start();
			while (a.isRunning()) {
				QApplication::processEvents();
				QThread::usleep(1);
			}

			Pk3List->setCurrentRow(0);
			SelectPk3();
		}


		void SendFile(); //== 1 is upload, 0 is download ==|

		void SelectPk3() {
			if (Thread == NULL && Busy == false) {
				Busy = true;
				Title->setText(Pk3List->currentItem()->text());

				LogoSpin->show();
				LogoSpin->movie()->start();
				Description->hide();

				ToggleButton->hide();
				UploadButton->hide();
				DownloadButton->hide();
				RemoveButton->hide();


				QString Buffer = "";

				SelectThread a(Title->text(),&Buffer);
				a.start();
				
				while (a.isRunning()) {
					QApplication::processEvents();
					QThread::usleep(1);
				}

				Description->setText(Buffer);

				UpdateButtons b(ToggleButton, UploadButton, DownloadButton, RemoveButton, Title);
				b.start();

				while (b.isRunning()) {
					QApplication::processEvents();
					QThread::usleep(1);
				}


				LogoSpin->hide();
				Title->show();
				Description->show();

				Busy = false;
			}
		}

		void Toggle() {

			if (ToggleButton->text() == "Enable") {
				QFile::rename(
					GamePath + Prefix + Title->text() + ".pk3.disabled",
					GamePath + Prefix + Title->text() + ".pk3");

				ToggleButton->setText("Disable");
				Pk3List->currentItem()->setIcon(QIcon(":Downloaded"));
			} 
			else {
				QFile::rename (
					GamePath + Prefix + Title->text() + ".pk3",
					GamePath + Prefix + Title->text() + ".pk3.disabled"  );

				ToggleButton->setText("Enable");
				Pk3List->currentItem()->setIcon(QIcon(":Disabled"));
			}
		}

		void RemovePk3() {

			if (ToggleButton->text() == "Enable")
				QFile::remove(GamePath + Prefix + Title->text() + ".pk3.disabled");

			else 
				QFile::remove(GamePath + Prefix + Title->text() + ".pk3");


			QPixmap Empty(30, 30);
			Empty.fill(Qt::transparent);

			Pk3List->currentItem()->setIcon(QIcon(Empty));

			//----- Show the correct buttons --|
			DownloadButton -> show();
			UploadButton   -> hide();
			ToggleButton   -> hide();
			RemoveButton   -> hide();

			}

		void PauseDownload() { if (Thread != NULL) { Thread->Toggle(); }}

		void GoBack() {
			this->FadeOut();
			Link->show();
		}

};

#endif