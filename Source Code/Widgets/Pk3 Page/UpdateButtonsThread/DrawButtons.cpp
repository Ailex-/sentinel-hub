#include "UpdateButtons.h"

UpdateButtons::UpdateButtons(QPushButton* ToggleButton, QPushButton* UploadButton, QPushButton* DownloadButton, QPushButton* RemoveButton, QLabel* Title) {

	this -> ToggleButton	= ToggleButton	 ;
	this -> UploadButton	= UploadButton	 ;
	this -> DownloadButton	= DownloadButton ;
	this -> RemoveButton	= RemoveButton	 ;
	this -> Title			= Title			 ;

}

void UpdateButtons::run(){

		bool IsOwner = false;
		int LocalVersion;
		int RemoteVersion = RemotePk3Version(Title->text());
		DownloadButton->hide();

		if (QFile::exists(GamePath + Prefix + Title->text() + ".pk3.disabled")) {
			QString Suffix = ".pk3.disabled";
			ToggleButton->setText("Enable");
			LocalVersion = LocalPk3Version(1, Title->text().toStdString());
			}
		else if(QFile::exists(GamePath + Prefix + Title->text() + ".pk3")){
			QString Suffix = ".pk3";
			ToggleButton->setText("Disable");
			LocalVersion = LocalPk3Version(0, Title->text().toStdString());
		}
		else {
			DownloadButton->show();
			return;
		}

		DownloadButton->setText("Install");

		//----- Check if User is Owner --|
		QStringList Owners = QString::fromStdString(CommandSSH("cat Pk3/" + Title->text().replace(" ", "_").toStdString() + "/Owner")).split("\n");
		Owners.removeAll("");
		
		for (int i = 0; i < Owners.size(); i++) { 
			if (User.toLower() == Owners[i].toLower()) { IsOwner = true; break; }
		}


		if (IsOwner)
			UploadButton->show();

		ToggleButton->show();
		RemoveButton->show();
	
		if (LocalVersion < RemoteVersion)
			DownloadButton->show();


}