#ifndef SENTINEL_UPPDATE_BUTTONS_THREAD_H
#define SENTINEL_UPPDATE_BUTTONS_THREAD_H

#include <QThread>
#include <QPushButton>

#include "../../../Ssh/ExecuteCommandSSH.h"
#include "../Functions/Pk3Versions/Pk3Versions.h"

extern QString User;
extern QString Prefix;

class UpdateButtons : public QThread {

	public:
		UpdateButtons(QPushButton* ToggleButton, QPushButton* UploadButton, QPushButton* DownloadButton, QPushButton* RemoveButton, QLabel* Title);


		void run();

	private:
		QPushButton * ToggleButton	 ;
		QPushButton * UploadButton	 ;
		QPushButton * DownloadButton ;
		QPushButton * RemoveButton	 ;
		QLabel      * Title			 ;

};


#endif // !SENTINEL_UPPDATE_BUTTONS_THREAD_H
