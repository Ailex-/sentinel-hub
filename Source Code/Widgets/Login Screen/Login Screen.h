#ifndef SENTINEL_HUB_LOGIN_SCREEN_H
#define SENTINEL_HUB_LOGIN_SCREEN_H

#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QPropertyAnimation>
#include <QApplication>
#include <QThread>

#include "../../Errors/Handler.h"

#include "../../Functions/Macros/DraggableWindow.h"
#include "../../Functions/Macros/FadeIn.h"
#include "../../Functions/Macros/WrapperLayout.h"

#include "../../Functions/ParseStyleSheet.h"
#include "../../Functions/CenteredLayout.h"
#include "../../Functions/CheckLocalSettings.h"

#include "../../Ssh/AuthThreadSSH.h"
#include "../../Ssh/ExecuteCommandSSH.h"

#include "../Download Update/Download Update.h"
#include "../Welcome Page/Welcome Page.h"
#include "../Menu Page/Menu Page.h"

extern QString User;
extern QString Pass;
extern QStringList Groups;
extern int Version;

class LoginPage : public QWidget {

	Q_OBJECT;
	DRAGGABLE_WINDOW;

	public:
		LoginPage();

	private:
		QLabel * Status = new QLabel("  ",this);

		QLineEdit * UserInput = new QLineEdit("", this);
		QLineEdit * PassInput = new QLineEdit("", this);
		
		QPushButton * ExitBtn  = new QPushButton("Quit", this);
		QPushButton * LoginBtn = new QPushButton("Login", this);

		bool Lock = false;

		void CheckUpdates();
		

	private slots:

		void Login();
		void FadeIn()  { FADE_IN_ANIMATION; }
		void FadeOut() { FADE_OUT_ANIMATION; }
		

};

#endif // LOGIN_PAGE_H