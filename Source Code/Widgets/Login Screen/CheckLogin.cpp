#include "Login Screen.h"

void LoginPage::Login(){

	// Prevents from spamming the login button and sending too many requests
	if (!Lock) { 
		Lock = true;

		Status->setText("Connecting...");

		AuthThreadSSH Thread(Status, UserInput->text(), PassInput->text().toStdString());
		Thread.start();


		// Refresh the GUI while the authentication is happening (otherwise it would freeze)
		while (!Thread.isFinished()) { QApplication::processEvents(); QThread::msleep(1); }


		// The thread updates the Label with the result of the authentication
		if (Status->text() == "Success!") {

			User = UserInput->text();
			User[0] = UserInput->text()[0].toUpper();

			Status->setText("Looking for Updates");

			Pass = PassInput->text();

			CheckUpdates();
		}

		// >> Authentication attempt has ended so another attempt can be made
		else { Lock = false; }
	}
}



void LoginPage::CheckUpdates() {
	Status->setText("Gathering User Info");
	Groups = QString::fromStdString(CommandSSH("groups")).remove('\n').split(' ');

	this->FadeOut();

	// If the server has a newer version, download it
	if (Version < std::stoi(CommandSSH("cat Client/Version"))) { new DownloadUpdate; }
	
	else {
		// If this is a first boot show a welcome screen
		int rc = CheckLocalSettings();
		if (rc == 100) { new MenuPage; }
		else { new WelcomePage; }
	}

}
