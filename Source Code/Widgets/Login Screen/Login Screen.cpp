#include "Login Screen.h"

LoginPage::LoginPage() {

	this->FadeIn();
	this->setFixedSize(800, 400);


	//	>> Fonts

	UserInput -> setFont(QFont("Dumbledor 3", 12)); UserInput->setPlaceholderText("Username");
	PassInput -> setFont(QFont("Dumbledor 3", 12)); PassInput->setPlaceholderText("Password");
	Status    -> setFont(QFont("Alegreya SC", 10));

	ExitBtn  -> setFont(QFont("Dumbledor 3", 12));	 ExitBtn  -> setFixedWidth(100);  ExitBtn  -> setObjectName("ButtonSmall");
	LoginBtn -> setFont(QFont("Dumbledor 3", 12));	 LoginBtn -> setFixedWidth(140);  LoginBtn -> setObjectName("ButtonSmall");
	

	//	>> Makes the password input line hide the characters with *
	PassInput->setEchoMode(QLineEdit::Password);


	UserInput->setFixedWidth(250);
	PassInput->setFixedWidth(250);

	//	>> Layouts


	QVBoxLayout* MainLayout = new QVBoxLayout(this);

	MainLayout->addSpacing(120);
	
	MainLayout->addLayout(HCentered(UserInput));	MainLayout->addSpacing(15);	
	MainLayout->addLayout(HCentered(PassInput));	MainLayout->addSpacing(22);
	MainLayout->addLayout(HCentered(Status   ));	MainLayout->addSpacing(10);
	MainLayout->addLayout(HCentered(LoginBtn ));	MainLayout->addSpacing( 6);
	MainLayout->addLayout(HCentered(ExitBtn  ));
	MainLayout->addStretch();


	WRAPPER_LAYOUT(MainLayout);

	this->setStyleSheet(Style(":LoginPage") + Style(":Button-Small"));




	connect(LoginBtn, SIGNAL(clicked()	), this,  SLOT( Login()		));
	connect(ExitBtn , SIGNAL(clicked()	), this,  SLOT( FadeOut()	));

	this->show();
}