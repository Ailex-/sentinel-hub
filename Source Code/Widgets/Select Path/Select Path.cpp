#include "Select Path.h"
#include <QFileSystemModel>

SelectPath::SelectPath(QWidget* Link) {

	this->FadeIn();

	this->Link = Link;

	// >> Stylesheet and Settings
	this->setFixedSize(650, 380);

	FileURL -> setFixedWidth(300);
	FolderView  ->   setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);
	FolderView  -> setHorizontalScrollMode(QAbstractItemView::ScrollPerPixel);
	/*
	
	FolderView    -> setObjectName("DirView"   ) ;
	FolderView	  -> setObjectName("FolderView") ;
	*/

	// >> This "Filler" Widget is used to paint the bottom part of the Disk buttons, otherwise that part would be empty/transparent
	QWidget * Filler = new QWidget(this);
		Filler -> setObjectName("Filler");
		Filler -> setMinimumHeight(15);	// <-- Makes sure that there always is a round border at the bottom left


	// >> Is the vertical Row of Buttons where each button is a drive (like C:\ or D:\)
	QWidget * ButtonsList = new QWidget(this);
	QVBoxLayout * ButtonsListLayout = new QVBoxLayout();
		ButtonsListLayout -> setSpacing(0);
		ButtonsListLayout -> setContentsMargins(0,0,0,0);
		GetDisks(ButtonsListLayout);						// <-- Populates the DiskView Table to ButtonsListLayout
		ButtonsListLayout->addWidget(Filler);

		ButtonsList -> setLayout(ButtonsListLayout);


	QLabel * Title = new QLabel("Select GameData folder", this);
		Title->setFont(QFont("Dumbledor 3",18));
	
	QLabel * PathLabel = new QLabel("Path : ",this);
		PathLabel->setFont(QFont("Dumbledor 3", 13));

	FileURL -> setFont(QFont("Source Code Pro Medium", 10));
	//FolderView -> setFont(QFont("Source Code Pro Medium", 10));

	// It's the top row that has the Path Bar and the back a dir button
	QHBoxLayout * Row2 = new QHBoxLayout;
		Row2->addSpacing(50);
		Row2->addWidget(PathLabel);
		Row2->addWidget(FileURL);
		Row2->addSpacing(30);
		Row2->addWidget(BackDir);
		Row2->addSpacing(50);


	// The widget that is shown at the center and contains both the directory list and drives button
	QHBoxLayout * TablesLayout = new QHBoxLayout;
		TablesLayout -> addSpacing(20)			;
		TablesLayout -> addWidget(ButtonsList)	;
		TablesLayout -> addWidget(FolderView)	;
		TablesLayout -> addSpacing(20)			;
		TablesLayout -> setSpacing(0)			;


	// >> Last row, it has the exit button and the select folder button
	QHBoxLayout * ExitLayout = new QHBoxLayout;
		ExitLayout->addStretch();
		ExitLayout->addWidget(SelectFolderButton);
		ExitLayout->addSpacing(6);
		ExitLayout->addWidget(ExitButton);
		ExitLayout->addSpacing(13);



	// >> Main Layout

	QVBoxLayout * MainLayout = new QVBoxLayout;
		MainLayout -> addSpacing(12);
		MainLayout -> addLayout(HCentered(Title)); // Title
		MainLayout -> addLayout(Row2)         ;	   // Path Bar
		MainLayout -> addSpacing(10)		  ;
		MainLayout -> addLayout(TablesLayout) ;
		MainLayout -> addSpacing(25)		  ;
		MainLayout -> addLayout(ExitLayout)   ;
		MainLayout -> addSpacing(6)			  ;

	WRAPPER_LAYOUT(MainLayout);

	this->show();


	UpdateTable(); // <-- Populates the table and selects the 1st drive it finds


	ExitButton	-> setObjectName("ButtonSmall");	ExitButton -> setFont(QFont("Dumbledor 3", 12));
	BackDir		-> setObjectName("ButtonSmall");	BackDir	   -> setFont(QFont("Dumbledor 3", 12));
	SelectFolderButton->setObjectName("ButtonSmall");  SelectFolderButton  -> setFont(QFont("Dumbledor 3",12));

	this->setStyleSheet(Style(":SelectPath") + Style(":Button-Small"));

	this->setAttribute(Qt::WA_DeleteOnClose, true);

	connect(ExitButton			, SIGNAL(clicked()								),	this,  SLOT(FadeOut()		)); // Exit Button
	connect(FolderView			, SIGNAL(itemDoubleClicked(QListWidgetItem*)	),	this,  SLOT(UpdateDir()		)); // When a directory is double clicked on the table execute UpdateDir()
	connect(BackDir				, SIGNAL(clicked()								),	this,  SLOT(GetBackDir()	));
	connect(SelectFolderButton	, SIGNAL(clicked()								),	this,  SLOT(SelectFolder()	));

}