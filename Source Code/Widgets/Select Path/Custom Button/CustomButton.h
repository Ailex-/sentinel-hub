#ifndef SELECT_PATH_CUSTOM_BUTTON_H
#define SELECT_PATH_CUSTOM_BUTTON_H

#include <QPushButton>

// Custom QPushButton class that holds a Path variable and it's "plug n play"

class CustomButton : public QPushButton {
    Q_OBJECT
    public:
        CustomButton(QString Name = "", QString GivenPath ="",QWidget * ReciverObject =NULL , QString ObjectName="", int index=0);
        QString Path = "";
};


#endif // SELECT_PATH_CUSTOM_BUTTON_H