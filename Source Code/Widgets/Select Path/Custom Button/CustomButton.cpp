#include "CustomButton.h"

CustomButton::CustomButton(QString Name, QString GivenPath, QWidget * ReciverObject, QString ObjectName, int Index) {
	this->setText(Name);
	Path = GivenPath;
	this->setObjectName(ObjectName);
	this->setFont(QFont("Source Code Pro Medium", 10));
	this->setFixedSize(70,30);
	this->setAutoExclusive(1);
	this->setCheckable(1);
	if (Index == 0) { this->setChecked(true); }
	connect(this, SIGNAL(clicked()), ReciverObject, SLOT(ChangeDrive()));
}