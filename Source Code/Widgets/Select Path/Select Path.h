#ifndef SENTINEL_SELECT_PATH_H
#define SENTINEL_SELECT_PATH_H

#include <QWidget>

#include <QLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QListWidget>
#include <QLabel>
#include <QStorageInfo>
#include <QList>

#include <QFile>
#include <QTextStream>

#include "../../Functions/CenteredLayout.h"
#include "../../Functions/ParseStyleSheet.h"
#include "../../Functions/ValidatePath.h"
#include "Custom Button/CustomButton.h"

#include "../../Functions/Macros/FadeIn.h"
#include "../../Functions/Macros/DraggableWindow.h"
#include "../../Functions/Macros/WrapperLayout.h"

#include "../../Errors/Handler.h"


#include <QPropertyAnimation>

extern QString GamePath;

class SelectPath : public QWidget
{


	Q_OBJECT;
	DRAGGABLE_WINDOW;

	public:
		SelectPath(QWidget* Parent = NULL);

	private:
		QListWidget * FolderView =  new QListWidget(this);

		QPushButton	* ExitButton		 =  new QPushButton("Exit",this);
		QPushButton	* BackDir			 =  new QPushButton("Back",this);
		QPushButton	* SelectFolderButton =  new QPushButton("Select Folder",this);

		QLineEdit	* FileURL =  new QLineEdit(this);
		QDir * Path = new QDir;

		QWidget* Link;

		void UpdateTable();

	// >> Populates the given Layout with all the mounted drives it detects
		void GetDisks(QVBoxLayout* Layout);

		void SavePath();
	public slots:
		void FadeIn() {
			FADE_IN_ANIMATION;
			move(
				this->screen()->size().width() / 2 - this->width() / 2,
				this->screen()->size().height() / 2 - this->height() / 2
			);
		}

	private slots:
		void ChangeDrive() {
			// # Next level-bs, this line makes a copy of the object that connected to the slot, thus
			// it's possible to read data from the sender object
			// [ CustomButton* buttonSender = qobject_cast<CustomButton*>(sender()); ]

			Path->cd(qobject_cast<CustomButton*>(sender())->Path);
			FileURL->setText(Path->path());

			UpdateTable();

		}

		void UpdateDir() {
			
			Path->cd(FolderView->currentItem()->text());
			FileURL->setText(Path->path());

			UpdateTable();

		}

		void GetBackDir() {

			Path->cdUp();
			FileURL->setText(Path->path());

			UpdateTable();
		}

		void SelectFolder() {

			// If no folder is selected validate the URL path
			if (FolderView->currentItem() != NULL) {
				Path->cd(FolderView->currentItem()->text());
				FileURL->setText(Path->path());

				int ReturnCode = ValidatePath(Path->path());
				if ( ReturnCode != 100) { new Error(900); }
				else { GamePath = FileURL->text(); SavePath(); FadeOut(); }
		
			}

			// Otherwise validate the folder with the selection
			else {
				int ReturnCode = ValidatePath(FileURL->text());
				if (ReturnCode != 100) { new Error(900); }
				else { 
					GamePath = FileURL->text();
					SavePath();
					FadeOut();
					Link->show();
				}
			}
		};



		void FadeOut() {

			QPropertyAnimation* FadeOut = new QPropertyAnimation(this, "windowOpacity");
			FadeOut->setDuration(200);
			FadeOut->setStartValue(1);
			FadeOut->setEndValue(0);
			FadeOut->start();

			if (Link != NULL && Link->objectName() == "MenuPage") {
				connect(FadeOut, SIGNAL(finished()), Link, SLOT(FadeIn()));
				connect(FadeOut, SIGNAL(finished()), this, SLOT(close()));
				Link->show();
			}
		}

};

#endif
