#include "../Select Path.h"

void SelectPath::GetDisks(QVBoxLayout * Layout) {

	QList MountedVolumes = QStorageInfo::mountedVolumes();

	QString ObjectName = "DiskButtonTop";	// <-- Needed to separate the square buttons from the
											//	   1st button which is rounded at the top left.


	// >> Cycles all the volumes it detected, and it delets from memory empty cd-roms or similar
	for (int i = 0; i < MountedVolumes.size(); i++) {
		if (MountedVolumes[i].device().length() == 0) MountedVolumes.removeAt(i); // <-- Empty thing
		if (i > 0) { ObjectName = "DiskButton"; }
		Layout->addWidget(new CustomButton(MountedVolumes[i].displayName(),MountedVolumes[i].rootPath(),this,ObjectName,i));
	}


	// >> The [URL : C:\ ] at the top of the widget
	Path->cd(MountedVolumes[0].rootPath());
	FileURL->setText(Path->path());

}
