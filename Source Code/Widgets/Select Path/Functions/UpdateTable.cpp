#include "../Select Path.h"

void SelectPath::UpdateTable() {

	FolderView->clear();										// <-- Clears the table where the directories will be listed


	QDir* directory = new QDir(FileURL->text());				// <-- Makes a new QDir at the path of top Path bar
	directory->setFilter(QDir::Dirs | QDir::NoDotAndDotDot);	// <-- Return all Dirs that are not '.' and '..'

	FolderView->addItems(directory->entryList());				// <-- Add them to the main table
	

	// Sets a custom size for each element inside the Folder View,
	// If the name is GameData or MBII also sets the text green,
	// that's jsut a neat feature to help find the directory.
	for (int i = 0; i < FolderView->count(); i++) {	
		FolderView->item(i)->setSizeHint(QSize(10, 20));

		if (FolderView->item(i)->text() == "GameData") {
			FolderView->item(i)->setForeground(QBrush(Qt::green));
		}
	}
}