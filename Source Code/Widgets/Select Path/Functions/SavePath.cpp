#include "../Select Path.h"

void SelectPath::SavePath() {

	// If the config folder isn't there, make it
	if (QDir(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation)).exists() == false) {
		QDir().mkdir(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation)); 
	}
	
	QFile::remove(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/MBIIPath.txt");
	
	QFile file(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + "/MBIIPath.txt");
		if (file.open(QIODevice::ReadWrite)) {
			QTextStream stream(&file);
			stream << GamePath;
	}

	file.close();
}