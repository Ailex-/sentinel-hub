#ifndef SENTINEL_MENU_PAGE_H
#define SENTINEL_MENU_PAGE_H

#include <QLabel>
#include <QLayout>
#include <QLineEdit>
#include <QPropertyAnimation>
#include <QPushButton>
#include <QScreen>

#include "../../Functions/Macros/DraggableWindow.h"
#include "../../Functions/Macros/FadeIn.h"
#include "../../Functions/Macros/WrapperLayout.h"

#include "../../Functions/ParseStyleSheet.h"
#include "../../Functions/CenteredLayout.h"

#include "../../Functions/CheckLocalSettings.h"

#include "../Select Path/Select Path.h"
#include "../Pk3 Page/Pk3 Page.h"
#include "../Accounts Page/AccountPage.h"

class MenuPage : public QWidget {

    Q_OBJECT
    DRAGGABLE_WINDOW

    public:
        MenuPage(bool hide = false);

    private:
        Pk3Page     *  mem_pk3     = NULL ;
        SelectPath  *  mem_path    = NULL ;
        AccountPage *  mem_account = NULL ;


    public slots:
         
        void FadeIn() {
            this->setWindowOpacity(0);
            FADE_IN_ANIMATION;

            move(
                this->screen()->size().width()/2  - this->width()/2,
                this->screen()->size().height()/2 - this->height()/2
            );
        }

        void FadeOut() {
            QPropertyAnimation* FadeOut = new QPropertyAnimation(this, "windowOpacity");
                FadeOut->setDuration(200);
                FadeOut->setStartValue(1);
                FadeOut->setEndValue(0);
                FadeOut->start();
            connect(FadeOut, SIGNAL(finished()), this, SLOT(hide()));
       }
        
        void Pk3() {
            FadeOut();
            if (mem_pk3 == NULL) { mem_pk3 = new Pk3Page(this); }
            else { mem_pk3->show(); mem_pk3->FadeIn(); }
         }

        void NewPath() {
            FadeOut();
            if (mem_path == NULL) { mem_path = new SelectPath(this); }
            else { mem_path->show(); mem_path->FadeIn(); }
        }


        void Account() {
            FadeOut();
            if (mem_account == NULL) { mem_account = new AccountPage(this); }
            else { mem_account->show(); mem_account->FadeIn(); }
        }
            


        void Options() {
            FadeOut();
            new SelectPath(this);
        }

        void FadeOutQuit() { FadeOut(); QApplication::quit(); }

};

#endif
