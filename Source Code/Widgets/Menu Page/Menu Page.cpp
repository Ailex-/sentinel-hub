#include "Menu Page.h"

MenuPage::MenuPage(bool hide) {

	this->setFixedSize(450, 600);
	this->setObjectName("MenuPage");

	QPushButton * PkrButton		 = new QPushButton("Assets Management" , this);	PkrButton	  -> setObjectName("ButtonBig");		PkrButton	   -> setFont(QFont("Dumbledor 3", 14));
	QPushButton * ServerButton	 = new QPushButton("Server Management", this);	ServerButton  -> setObjectName("ButtonBig");		ServerButton   -> setFont(QFont("Dumbledor 3", 14));
	QPushButton * AccountsButton = new QPushButton("Manage Accounts"  , this);	AccountsButton-> setObjectName("ButtonBig");		AccountsButton -> setFont(QFont("Dumbledor 3", 14));
	QPushButton * OptionsButton  = new QPushButton("Change Path"	  , this);	OptionsButton -> setObjectName("ButtonBig");		OptionsButton  -> setFont(QFont("Dumbledor 3", 14));
	QPushButton * ExitButton	 = new QPushButton("Exit"			  , this);	ExitButton	  -> setObjectName("ButtonBigExit"  );	ExitButton	   -> setFont(QFont("Dumbledor 3", 14));

	QVBoxLayout * MainLayout = new QVBoxLayout(this);
		MainLayout->addSpacing(240);
		MainLayout->addLayout(HCentered(PkrButton		));	MainLayout->addSpacing(6);
		MainLayout->addLayout(HCentered(ServerButton	));	MainLayout->addSpacing(6);
		MainLayout->addLayout(HCentered(AccountsButton	));	MainLayout->addSpacing(6);
		MainLayout->addLayout(HCentered(OptionsButton	));	MainLayout->addSpacing(20);
		MainLayout->addLayout(HCentered(ExitButton		));
		MainLayout->addStretch();


	WRAPPER_LAYOUT(MainLayout);


	this->setStyleSheet(Style(":Button-Big")+Style(":MenuPage"));
	
	connect(PkrButton	  , SIGNAL( clicked()  ), this,  SLOT( Pk3()		 ));
	connect(ServerButton  , SIGNAL( clicked()  ), this,  SLOT( RCon()		 ));
	connect(AccountsButton, SIGNAL( clicked()  ), this,  SLOT( Account()     ));
	connect(OptionsButton , SIGNAL( clicked()  ), this,  SLOT( Options()	 ));
	connect(ExitButton	  , SIGNAL( clicked()  ), this,  SLOT( FadeOutQuit() ));

	if (hide == false) {
		this->FadeIn();
		this->show();
	}
}
