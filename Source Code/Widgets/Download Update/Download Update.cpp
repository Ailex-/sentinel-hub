#include "Download Update.h"

DownloadUpdate::DownloadUpdate() {

	this->setFixedSize(500, 250);
	this->FadeIn();

	Progress -> setWordWrap(true);
	Progress -> setFixedWidth(340);
	Progress -> setAlignment(Qt::AlignHCenter);

	Title	   -> setFont(QFont("Dumbledor 3", 18));
	Progress   -> setFont(QFont("Dumbledor 3", 13));
	Size	   -> setFont(QFont("Dumbledor 3", 12));
	ExitButton -> setFont(QFont("Dumbledor 3", 12));


	Bar->setFixedSize(420,25);
	Bar->setTextVisible(false);
	Bar->setMask(QPixmap(":Small-Box-Mask").mask());


	QHBoxLayout* BarLayout = new QHBoxLayout();
		BarLayout->addStretch();
		BarLayout->addWidget(Bar);
		BarLayout->addStretch();

	QVBoxLayout* MainLayout = new QVBoxLayout(this);

		MainLayout->addSpacing(65);
		MainLayout->addLayout(HCentered(Title));
		MainLayout->addSpacing(7);
		MainLayout->addLayout(HCentered(Progress));
		MainLayout->addSpacing(4);
		MainLayout->addLayout(HCentered(Size));
		MainLayout->addStretch();
		MainLayout->addLayout(BarLayout);
		MainLayout->addLayout(HCentered(ExitButton));
		MainLayout->addSpacing(10);

	WRAPPER_LAYOUT(MainLayout);

	ExitButton->setObjectName("ButtonSmall");
	this->setStyleSheet(Style(":Small-Box") + Style(":Bar") +Style(":Button-Small"));

	this->show();

	Download();
}


void DownloadUpdate::Download() {

	size_t math = 0;

	QFile::rename(QFileInfo(QCoreApplication::applicationFilePath()).fileName(), "Test.exe");

	Thread = new FileSender(0, "Sentinel Hub.exe", "Client/Application.exe", &NumberSize, &CurrentSize, &Speed);
	Thread->start(QThread::LowestPriority);

	while (Thread->isRunning()) {

		QApplication::processEvents();

		if (NumberSize == 0) { Bar->setValue(0); }	// Cannot divide by zero
		else {
			math = CurrentSize;
			math = math * 100;						// It's needed to work with size_t in case the filesize > what "int" supports
			math = math / NumberSize;				//

			Bar->setValue(static_cast<int>(math));	// It's safe to cast from size_t to int because percentage range from 0 to 100
			Bar->repaint();
		}

		Size->setText(QString::number(NumberSize/1048576) + " / Mb");
		Progress->setText(QString::number(CurrentSize / 1048576));


		QThread::msleep(10);
	}


	QThread::msleep(100);

	Bar->hide();
	Size->hide();
	
	Bar->deleteLater();
	Size->deleteLater();

	Title->setText("Update Finished!");
	Progress->setText("The program has been updated, click the exit button <br> then reopen the application.");

	connect(ExitButton, SIGNAL( clicked()), this, SLOT( FadeOut()) );
}