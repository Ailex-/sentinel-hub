#ifndef SENTINEL_HUB_DOWNLOAD_UPDATE_H
#define SENTINEL_HUB_DOWNLOAD_UPDATE_H

#include <QProgressbar>
#include <QLabel>
#include <QPainter>
#include <QApplication>
#include <QProcess>
#include <QFileInfo>

#include "../../Errors/Handler.h"

#include "../../Functions/Macros/DraggableWindow.h"
#include "../../Functions/Macros/FadeIn.h"
#include "../../Functions/Macros/WrapperLayout.h"

#include "../../Functions/ParseStyleSheet.h"
#include "../../Functions/CenteredLayout.h"

#include "../../Ssh/FileSender/FileSender.h"


class DownloadUpdate : public QWidget {

	Q_OBJECT;
	DRAGGABLE_WINDOW;

	public:
		DownloadUpdate();
		
	private:
		QProgressBar * Bar = new QProgressBar(this);
		QLabel * Title	   = new QLabel("Downloading Update",this);
		QLabel * Progress  = new QLabel("0%",this);
		QLabel * Size	   = new QLabel("0 / 0 Mb",this);

		QPushButton * ExitButton = new QPushButton("Exit", this);

		void Download();

		size_t NumberSize = 0;
		size_t CurrentSize = 0;
		size_t Speed = 0;

		FileSender * Thread = NULL;

	private slots:

		void FadeIn()  { FADE_IN_ANIMATION;  }
		void FadeOut() { FADE_OUT_ANIMATION; }
};

#endif