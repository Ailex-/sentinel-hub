#ifndef EXECUTE_COMMAND_SSH_H
#define EXECUTE_COMMAND_SSH_H

#include <libssh/libssh.h>
#include <string>

#include "../Errors/Handler.h"

extern ssh_session Session;

std::string CommandSSH(std::string Command);

#endif //EXECUTE_COMMAND_SSH_H