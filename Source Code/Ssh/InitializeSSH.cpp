#include "InitializeSSH.h"

std::string InitializeSSH(std::string User, std::string Pass, std::string IP, int Port, int Verbosity, int Zlib_Compression) {

	ssh_init();

	Session = NULL;		 // Clears the variable, just to prevent
	Session = ssh_new(); // any mistakes/errors


	if (Session == NULL) { return "Internal Error."; }

	int rc = 0;
	// 	SSH Options Configuration
	
	ssh_options_set(Session, SSH_OPTIONS_HOST, IP.c_str());	
	ssh_options_set(Session, SSH_OPTIONS_PORT, &Port);
	ssh_options_set(Session, SSH_OPTIONS_LOG_VERBOSITY, &Verbosity);
	ssh_options_set(Session, SSH_OPTIONS_KEY_EXCHANGE, "curve25519-sha256");
	ssh_options_set(Session, SSH_OPTIONS_COMPRESSION, "zlib");
	ssh_options_set(Session, SSH_OPTIONS_COMPRESSION_LEVEL, &Zlib_Compression);
	ssh_options_set(Session, SSH_OPTIONS_HMAC_C_S, "aes192-ctr");
	ssh_options_set(Session, SSH_OPTIONS_HMAC_S_C, "aes192-ctr");


	/* Tries to connect the session 				*/
	
	rc = ssh_connect(Session);  if (rc != SSH_OK) {
		if (ssh_get_error_code(Session) == 2) return "Connection Timed out.";
		else return "Connection Error.";
	}


	/* Tries to autenthicate                        */
	rc = ssh_userauth_password(Session, User.c_str(), Pass.c_str());
	if (rc != SSH_AUTH_SUCCESS) { return "Authentication Failed."; }
	else { return "Success!"; }
}