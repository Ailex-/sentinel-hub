#ifndef SEN_THREADED_COMMAND_SSH_H
#define SEN_THREADED_COMMAND_SSH_H

#include <libssh/libssh.h>
#include <string>

#include "../../Errors/Handler.h"


extern ssh_session Session;

std::string CommandSSH_TH(std::string Command);

#endif // !SEN_THREADED_COMMAND_SSH_H
