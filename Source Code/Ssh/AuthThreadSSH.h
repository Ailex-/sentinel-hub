#ifndef AUTHORIZATION_THREAD_SSH_H
#define AUTHORIZATION_THREAD_SSH_H

#include <QThread>
#include <QLabel>
#include <string>

#include "InitializeSSH.h"

//				This thread is hacky as heck
//
//	Since run() cannot take parameters, when the thread is created parameters are passed to it,
//	those parameters are then stored in memory as private members of the class.
//
//	These members are a sort of "buffer", and since the UI element (the QLabel) is a pointer,
//	all changes made to it will show up to the GUI.
//
//	The [User] variable is different since it's a global variable and that's why it's assigned but
//	not in the "buffer", since, welp, it has it's own memory.


extern ssh_session Session;
extern     QString User;

class AuthThreadSSH : public QThread
{
	public :
		
		void run(){
			Label->setText(QString::fromStdString(InitializeSSH("hub-" + User.toStdString(), Password)));
			this->quit();
		}


		AuthThreadSSH(QLabel* BufferLabel, QString BufferUser, std::string BufferPassword){
			Label	 = BufferLabel	  ;
			Password = BufferPassword ;
			User	 = BufferUser	  ; // Declated globally
		}

	private :
		QLabel * Label;
		std::string Password;
};

#endif //AUTHORIZATION_THREAD_SSH_H
