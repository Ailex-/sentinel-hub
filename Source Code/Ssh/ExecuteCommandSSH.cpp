#include "ExecuteCommandSSH.h"

std::string CommandSSH(std::string Command) {

	 int rc;
	 char Buffer[512];
	 std::string Output;

	 ssh_channel Channel;
	 Channel = ssh_channel_new(Session);
		if( Channel == NULL) { new Error(200); return ""; }
	 rc = ssh_channel_open_session(Channel);
		if(rc != SSH_OK) { new Error(201); return ""; }
	 rc = ssh_channel_request_exec(Channel, Command.c_str());
		if(rc != SSH_OK) { new Error(202); return ""; }

	 // >> Reads the number of bytes from the channel to the buffer
	 const int nbytes = ssh_channel_read(Channel, Buffer, sizeof(Buffer), 0);

	 if (nbytes > 512) {
		 new Error(203);
		 return "";
	 }

	 // >> Copy into output every chatacter 
	 for (int i = 0; i < nbytes; i++) { Output = Output + Buffer[i]; }

	 ssh_channel_close(Channel);

	 return Output;

}