#ifndef INITIALIZE_SSH_H
#define INITIALIZE_SSH_H

#include <libssh/libssh.h>
#include <string>


extern std::string Server ;
extern ssh_session Session;

std::string InitializeSSH(std::string User, std::string Pass, std::string IP = Server, int Port = 22, int Verbosity = 3, int Zlib_Compression = 1);


#endif //INITIALIZE_SSH_H