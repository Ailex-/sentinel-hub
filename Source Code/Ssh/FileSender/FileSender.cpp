#include "FileSender.h"

FileSender::FileSender(int Mode, std::string LocalPath, std::string RemotePath, size_t * Size, size_t * CurrentSize, size_t * Speed) {

	this->Mode		 = Mode		  ;
	this->LocalPath  = LocalPath  ;
	this->RemotePath = RemotePath ;

	this->Size		  = Size	    ;
	this->CurrentSize = CurrentSize ;
	this->Speed		  = Speed		;

}


void FileSender::run() {

	int rc;
	if (Mode == 1) {
		rc = UploadFile();
		if (rc != 0)
			new Error(rc);
	}
	else		   { DownloadFile(); }
	
	this->quit();
}



void FileSender::Resume() {
	Sync.lock();
	Wait = false;
	Sync.unlock();
	pauseCond.wakeAll();
}

void FileSender::Pause() {
	Sync.lock();
	Wait = true;
	Sync.unlock();
}

void FileSender::Toggle() {
	if (Wait == true) { Resume(); }
	else			  { Pause() ; }
}