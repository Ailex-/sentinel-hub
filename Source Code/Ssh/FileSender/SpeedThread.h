#ifndef SPEED_THREAD_H
#define SPEED_THREAD_H

#include <QThread>

class SpeedThread : public QThread {
	
	private:
		size_t *Speed = 0;
		size_t *Bytes = 0;

	public:
		SpeedThread(size_t* Bytes, size_t* Speed);

		void run();


};

#endif // SPEED_THREAD_H
