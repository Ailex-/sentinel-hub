#include "FileSender.h"

int FileSender::DownloadFile(){
  ssh_scp scp;
  size_t bytes_read = 0;
  FILE* LocalFile;
  LocalFile = fopen(LocalPath.c_str(), "wb");

  std::string a = "./" + RemotePath;

  scp = ssh_scp_new(Session, SSH_SCP_READ, a.c_str());
  ssh_scp_init(scp);
  ssh_scp_pull_request(scp);
 
  *Size = ssh_scp_request_get_size(scp);

  char* F = (char*) malloc(*Size);

  int mode = ssh_scp_request_get_permissions(scp);

  ssh_scp_accept_request(scp);
 
  ssh_scp_pull_request(scp);

  SpeedThread Th(CurrentSize, Speed);
  Th.start(QThread::LowestPriority);

  while (*CurrentSize < *Size) {

	  Sync.lock();
	  if (Wait == true) { pauseCond.wait(&Sync); }
	  Sync.unlock();

	  bytes_read = ssh_scp_read(scp, buffer, BufferSize);
	  if (bytes_read < 0) { new Error(303); return 303; }
	  *CurrentSize = *CurrentSize + fwrite(buffer, sizeof(char), bytes_read, LocalFile);
  }

  Th.terminate();
  fclose(LocalFile);

  ssh_scp_close(scp);
  ssh_scp_free(scp);

  return 0;
}