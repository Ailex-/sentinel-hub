#ifndef SEN_FILE_SENDER_H
#define SEN_FILE_SENDER_H

/*	Class used to easily Upload or Download a single file to a remote machine usng SFTP		*/
/*	------------------------------------------------------------------------------------	*/

#include <QThread>
#include <QWaitCondition>
#include <QMutex>

#include <string>
#include <fstream>


//===== SSH Includes ==
#include <libssh/libssh.h>
#include <libssh/sftp.h>

#include <fcntl.h> //::: File permissions definitions


//===== Extra ==
#include "SpeedThread.h"
#include "../ExecuteCommandSSH.h"
#include "../../Errors/Handler.h"

extern ssh_session Session;

class FileSender : public QThread {
	
	private:
		//----- Used to make Pause/Resume --
		QMutex Sync;
		QWaitCondition pauseCond;

		//----- Data needed for Upload/Download --
		int Mode;									// 1 = Upload
		std::string LocalPath;
		std::string RemotePath;


		//----- Extra stuff, depends on the usecase --

		size_t *Size;
		size_t *CurrentSize;
		size_t *Speed;
		size_t *OldBytes;


		const int BufferSize = 8192;
		char  buffer[8192]; // 32768 In bytes, that is 16 kb

	public:
		FileSender(int Mode, std::string LocalPath, std::string RemotePath, size_t * Size, size_t * CurrentSize, size_t * Speed);

		void run();
		void Pause();
		void Resume();
		void Toggle();
		
		int DownloadFile();
		int UploadFile();


		bool Wait = false;

};

#endif //::FILE_SENDER_H::