#include "FileSender.h"


int FileSender::UploadFile() {

	ssh_scp scp;
	size_t bytes_sent = 0;

	std::string Path = RemotePath;

	scp = ssh_scp_new(Session, SSH_SCP_WRITE, RemotePath.substr(0, Path.length() - 9).c_str());
	ssh_scp_init(scp);
	ssh_scp_pull_request(scp);


	std::ifstream LocalFile(LocalPath, std::ios_base::binary);

	int access_type = O_WRONLY | O_CREAT | O_TRUNC;
	//mode_t perms = 0200 | 0400 | 0100 | 020 | 040 | 010 | 04 | 01;
	mode_t perms = 0771;

	*Size = LocalFile.tellg();				// Calculate file size
	LocalFile.seekg(0, std::ios::end);		//
	size_t end = LocalFile.tellg();			//

	*Size = end - *Size;

	LocalFile.seekg(0);					// Put pointer of file back on the 1st byte


	size_t RemainingBytes = *Size;

	ssh_scp_push_file(scp, RemotePath.c_str(), *Size, access_type);
	
	SpeedThread Th(CurrentSize, Speed);
	Th.start(QThread::LowestPriority);

	while (true) {

		Sync.lock();
		if (Wait == true) { pauseCond.wait(&Sync); }
		Sync.unlock();

		if (RemainingBytes > BufferSize) {
			LocalFile.read(buffer, BufferSize);
			RemainingBytes = RemainingBytes - BufferSize;

			bytes_sent = ssh_scp_write(scp, buffer, BufferSize);
			if (bytes_sent < 0) { return 305; }

			*CurrentSize = *Size - RemainingBytes;
		}
		else {
			LocalFile.read(buffer, RemainingBytes);
			bytes_sent = ssh_scp_write(scp, buffer, RemainingBytes);
			if (bytes_sent < 0) { return 305; }

			*CurrentSize = *Size - RemainingBytes;
			Th.terminate();
			break;
		}
	}

	LocalFile.close();

	ssh_scp_close(scp);
	ssh_scp_free(scp);

	return 0;
}