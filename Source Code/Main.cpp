
#include <QApplication>
#include <QFontDatabase>
#include <libssh/libssh.h>

#include "Errors/Handler.h"

#include "Widgets/Splash Screen/Splash Screen.h"
#include "Widgets/Login Screen/Login Screen.h"

//----- Global data, use it with extern --

ssh_session Session  = NULL ;
    QString User     = ""	;
    QString Pass     = ""   ;
QStringList Groups   = {}   ;
    QString GamePath = ""	; // It's the 'GameData' folder, doesn't has a '/' at the end!
		int	Version  = 1001	;
std::string Server   = #YOUR_SERVER_IP# ;

    QString Prefix = "/MBII/stnl-";


int main(int argc, char* argv[]) {

    QApplication App(argc, argv);
    App.setApplicationName("Sentinel Hub");


    QFontDatabase::addApplicationFont(":Dumbledor3");
    QFontDatabase::addApplicationFont(":Blackchanchery");


    //----- Update Finish --
    if (std::filesystem::exists("Old-Sentinel Hub.exe")) {
        if (remove("Old-Sentinel Hub.exe") != 0) { new Error(100); }
    }


    else { new LoginPage; }

    return App.exec();
}