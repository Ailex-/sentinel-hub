#include "ValidatePath.h"

int ValidatePath(QString Path) {
	
	// Check the Error handler for what return codes means
	
	if (Path.size() == 0) { return 900; }
	if (Path.size() < 16) { return 901; }
	
	if (QDir(Path).exists() == false) { return 902; }
	if (QDir(Path).isAbsolute() == false) { return 903; }

	if (Path.endsWith("/GameData") == false) { return 904; }

	if		(QFile(Path + "/mbii.x86.exe"    ).exists() == false) { return 905; }
	else if (QFile(Path + "/mbiided.x86.exe" ).exists() == false) { return 905; }
	else if (QFile(Path + "/mbiilauncher.exe").exists() == false) { return 905; }
	
	GamePath = Path;
	return 100;
}