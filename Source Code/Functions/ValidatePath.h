#ifndef VALIDATE_PATH_H
#define VALIDATE_PATH_H

#include <QDir>
#include <QLabel>
#include <QStandardPaths>

extern QString GamePath;

int ValidatePath(QString NewPath);

/*								Return Codes :
_________________________________________________________________________________________________________

	100 ~ Every check was successfull.
		. The MBIIPath variable gets updated


	800 ~ There folder where the options are saved is missing
		. It's normal if it's the first time the progam launches
		. Someone might wanna "hard reset" the settings by deleting the folder


	801 ~ There is no [ MBIIPath.txt ]
		. That file is generated automatically so someone had to delete it


	900 ~ The content of [ MBIIPath.txt ] is empty
		. Someone is trying to break the software
		. for security reasons the config file will be deleted


	901 ~ The path that's stored in [ MBIIPath.txt ] is too short to be a legitimate path
		. Someone is trying to break the software
		. for security reasons the config file will be deleted


	902 ~ The path that's stored in [ MBIIPath.txt ] doesn't exists
		. Someone is trying to break the software or tried to manually edit the config file
		. either way, for security reasons the config file will be deleted


	903 ~ The path that's stored in [ MBIIPath.txt ] is not an absolute path
		. Someone is trying to break the software or tried to manually edit the config file
		. either way, for security reasons the config file will be deleted


	904 ~ The path isn't a MBII Path (doesn't ends with [ GameData/MBII ] )
		. Someone is trying to break the software or tried to manually edit the config file
		. either way, for security reasons the config file will be deleted


	905 ~ There is a MBII file missing, thus the integrity of the game might by compromised
		. Possible steal of assets, program will block


*/
#endif // VALIDATE_PATH_H