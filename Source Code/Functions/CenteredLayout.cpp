#include "CenteredLayout.h"

QHBoxLayout * HCentered(QWidget* Widget, QWidget* Widget2){

	QHBoxLayout * Layout = new QHBoxLayout;

		Layout->addStretch();
		Layout->addWidget(Widget);
		if (Widget2 != NULL) { Layout->addWidget(Widget2); }
		Layout->addStretch();

	return(Layout);
}