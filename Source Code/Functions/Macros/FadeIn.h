#ifndef MACRO_FADE_IN_H
#define MACRO_FADE_IN_H

/*  This Macro adds a fade in animation to a QWidget, it has to be used inside a QWidget class and it  */
/*  needs the QPropertyAnimation class to be included!                */

#define FADE_IN_ANIMATION																    \
	QPropertyAnimation * FadeIn = new QPropertyAnimation(this, "windowOpacity");			\
		FadeIn->setStartValue(0.0);															\
		FadeIn->setEndValue(1.0);															\
		FadeIn->setDuration(500);															\
		FadeIn->start(QAbstractAnimation::DeleteWhenStopped);								
        

#define FADE_OUT_ANIMATION																	\
	QPropertyAnimation * FadeOut = new QPropertyAnimation(this, "windowOpacity");			\
		FadeOut->setDuration(200);															\
		FadeOut->setStartValue(1);															\
		FadeOut->setEndValue(0);															\
		FadeOut->start();																	\
																							\
	connect(FadeOut, SIGNAL(finished()), this, SLOT(deleteLater()));						

#endif // !MACRO_FADE_IN_H
