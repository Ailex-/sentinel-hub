#ifndef MACRO_WRAPPER_LAYOUT
#define MACRO_WRAPPER_LAYOUT

/*	This macro adds and applies a wrapper widget and layout to the selected widget, this was primarily  */
/*  primarily to add rounded borders to the main window (windows 10 era, dunno how it will work in	    */
/*	the future).																					    */
/*	It also handles the transparency and the removal of the default top bar, it takes a layout named    */
/*  MainLayout as a parameter and applies it to the wrapper, because, welp that's it's job.			    */
/*  Be aware that the wrapper doesn't come styled, but you can easily reference it with the "Wrapper"   */
/*  object name																						    */

#define WRAPPER_LAYOUT(LAYOUT_TO_WRAP)						\
	this->setAttribute(Qt::WA_TranslucentBackground);		\
	this->setWindowFlag(Qt::FramelessWindowHint);			\
															\
	QWidget * Wrapper = new QWidget;						\
	Wrapper->setObjectName("Wrapper");						\
	Wrapper->setLayout(LAYOUT_TO_WRAP);						\
															\
	QHBoxLayout * WrapperLayout = new QHBoxLayout;			\
	WrapperLayout->addWidget(Wrapper);						\
															\
	this->setLayout(WrapperLayout);

#endif // !MACRO_WRAPPER_LAYOUT
