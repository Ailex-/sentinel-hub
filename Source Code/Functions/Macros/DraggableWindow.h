#ifndef MACRO_DRAGGABLE_WINDOW_H
#define MACRO_DRAGGABLE_WINDOW_H

/*  This Macro makes any Widget draggable, it has to be used inside a QWidget class and it  */
/*  is a stand-alone macro, that is, it doens't have any dependecies at all.                */

#include <QMouseEvent>

#define DRAGGABLE_WINDOW                                                                \
    private:                                                                            \
        QPointF oldPos, delta;                                                          \
                                                                                        \
    protected:                                                                          \
        void mousePressEvent(QMouseEvent* evt) { oldPos = evt->globalPosition(); }      \
        void mouseMoveEvent(QMouseEvent* evt) {                                         \
            delta = evt->globalPosition() - oldPos;                                     \
            move(x() + delta.x(), y() + delta.y());                                     \
            oldPos = evt->globalPosition();                                             \
        }

#endif // !MACRO_DRAGGABLE_WINDOW_H