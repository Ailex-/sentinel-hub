#include "CheckLocalSettings.h"

// Was just too annoying to look at in the main fuction
// all this just to say [ %appdata%/local/Sentinel Hub/MBIIPath.txt ]
QString GetPath() {
	return QDir(QDir::cleanPath(QStandardPaths::writableLocation(QStandardPaths::AppConfigLocation) + QDir::separator() + "MBIIPath.txt")).path();
}



// Same as above, made code hard to read, all it does it reading the file
// MBIIPath.txt and returning the contents

std::string GetLine(QString Path) {
	std::ifstream Buffer(Path.toStdString());
	std::string Line;

	std::getline(Buffer, Line);
	Buffer.close();
	return Line;
}




// All this function has to do is to check if any local settings exists,
// the validation of the data is done separately
int CheckLocalSettings() {

	// >> If "%appdata%/Local/Sentinel Hub/" does not exists																			// ERROR 800
	if (QDir( QStandardPaths::writableLocation( QStandardPaths::AppConfigLocation )).exists() == false) { return 800; }
	else {

		QString	ConfigFilePath = GetPath(); // <-- Stupid long QT syntax

		// >> If MBIIPath.txt doesn't exists																							// ERROR 801
		if (QFile(ConfigFilePath).exists() == false) { return 801; }

		
		QString Path = QString::fromStdString(GetLine(ConfigFilePath)); // <-- Reads the file only if he's sure that it exists
		
		return ValidatePath(Path);
	}
}