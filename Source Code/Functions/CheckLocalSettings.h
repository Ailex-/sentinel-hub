#ifndef CHECK_LOCAL_SETTINGS_H
#define CHECK_LOCAL_SETTINGS_H

#include <QDir>
#include <QLabel>
#include <QStandardPaths>

// Used to read the User's config file
#include <fstream>

// Validates the path find inside MBIIPath.txt
#include "ValidatePath.h"

// Stores MBIIPath variable
extern QString GamePath;

int CheckLocalSettings();

#endif // CHECK_LOCAL_SETTINGS_H