#include "ParseStyleSheet.h"

QString Style(QString Path) {

    QFile Buffer(Path);
    Buffer.open(QFile::ReadOnly);

    // Reads the content line by line
    QByteArray output = Buffer.readAll();
    Buffer.close();

    return output;
}