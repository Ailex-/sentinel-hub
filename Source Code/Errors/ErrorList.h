#ifndef SENTINEL_HUB_ERROR_LIST_H
#define SENTINEL_HUB_ERROR_LIST_H

#define SEN_ERROR_100 "Could not delete the old version."
#define SEN_ERROR_101 "Could not rename the current executable."

#define SEN_ERROR_200 "Could not create an Ssh Channel."
#define SEN_ERROR_201 "Could not open an Ssh Channel."
#define SEN_ERROR_202 "Could not request an execution of a command over a Ssh Channel."
#define SEN_ERROR_203 "The output of a command sent with Ssh is too big to be stored on a buffer."

#define SEN_ERROR_300 "Could not create an SFTP Session."
#define SEN_ERROR_301 "Could not initialize an SFTP Conncetion."
#define SEN_ERROR_302 "Could not open a file on the server (SFTP)."
#define SEN_ERROR_303 "Could not read bytes from an SFTP File."
#define SEN_ERROR_304 "Could not properly close an SFTP Session."
#define SEN_ERROR_305 "Could not write bytes to an SFTP File."

#define SEN_ERROR_400 "Error uploading the file, the checksums are different: <br> either the file got corrupted or someone else is <br> updating the pk3 at the same exact time."
#define SEN_ERROR_401 "Error downloading the file, the checksums are different: <br> either the file got corrupted or an update was <br> taking place while downloading."

#define SEN_ERROR_500 "The Pk3 you're trying to upload is the same that it's on the server."
#define SEN_ERROR_501 "The version of the current Pk3 is invalid."
#define SEN_ERROR_502 "The Pk3 is missing the Version.txt file."

#define SEN_ERROR_600 "Insert both the passwords in the fields to update it."
#define SEN_ERROR_601 "The inserted passwords do not match."

#define SEN_ERROR_900 "The selected folder isn't correct."

#endif