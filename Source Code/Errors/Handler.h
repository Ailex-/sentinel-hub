#ifndef SENTINEL_HUB_ERROR_HANDLER_H
#define SENTINEL_HUB_ERROR_HANDLER_H

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QLayout>

#include <QPropertyAnimation>

#include "../Functions/Macros/DraggableWindow.h"
#include "../Functions/Macros/FadeIn.h"
#include "../Functions/Macros/WrapperLayout.h"
#include "../Functions/ParseStyleSheet.h"

#include "ErrorList.h"

class Error : public QWidget {

	Q_OBJECT;
	DRAGGABLE_WINDOW;

	public:
		Error(int ErrorCode, QString Argument="");

	private:
		QLabel * Title		 = new QLabel ;
		QLabel * Description = new QLabel ;

		QPushButton * ButtonExit = new QPushButton("Exit",this);

	public slots:
		void FadeIn () { FADE_IN_ANIMATION ; }
		void FadeOut() { FADE_OUT_ANIMATION; }
};

#endif