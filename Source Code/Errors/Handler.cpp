#include "Handler.h"

Error::Error(int ErrorCode, QString Argument) {

	this->setFixedSize(500, 250);

	Title		-> setFont(QFont("Dumbledor 3", 17));
	Description	-> setFont(QFont("Dumbledor 3", 13));
	ButtonExit	-> setFont(QFont("Dumbledor 3", 12));

	Title->setText("Error " + QString::number(ErrorCode));
	Description->setAlignment(Qt::AlignHCenter);
	Argument = "<br> [File Involved : " + Argument + "]";

	switch (ErrorCode) {
		case 100: Description->setText(SEN_ERROR_100); break; // Finish Update
		case 101: Description->setText(SEN_ERROR_101); break; 
		case 200: Description->setText(SEN_ERROR_200); break; // Ssh Channel
		case 201: Description->setText(SEN_ERROR_201); break;
		case 202: Description->setText(SEN_ERROR_202); break;
		case 203: Description->setText(SEN_ERROR_203); break;
		case 300: Description->setText(SEN_ERROR_300); break; // SFTP Subsystem
		case 301: Description->setText(SEN_ERROR_301); break;
		case 302: Description->setText(SEN_ERROR_302); break;
		case 303: Description->setText(SEN_ERROR_303); break;
		case 304: Description->setText(SEN_ERROR_304); break;
		case 400: Description->setText(SEN_ERROR_400 + Argument); break; // Failed MD5 Checksum
		case 401: Description->setText(SEN_ERROR_401 + Argument); break;
		case 500: Description->setText(SEN_ERROR_500 + Argument); break; // Uploading a pk3 that doesnt differ from what its in server
		case 501: Description->setText(SEN_ERROR_501 + Argument); break; // The [Version.txt] of the pk3 is invalid
		case 502: Description->setText(SEN_ERROR_502 + Argument); break; // There is no [Version.txt] inside the pk3
		case 600: Description->setText(SEN_ERROR_600); break;
		case 601: Description->setText(SEN_ERROR_601); break;
		case 900: Description->setText(SEN_ERROR_900); break; // Bad GameData folder
	}

	QVBoxLayout* MainLayout = new QVBoxLayout;

	MainLayout -> addSpacing(65);
	MainLayout -> addWidget(Title,       0, Qt::AlignHCenter);
	MainLayout -> addSpacing(7);
	MainLayout -> addWidget(Description, 0, Qt::AlignHCenter);
	MainLayout -> addStretch();
	MainLayout -> addWidget(ButtonExit,  0, Qt::AlignHCenter);
	MainLayout -> addSpacing(10);
	
	WRAPPER_LAYOUT(MainLayout);

	ButtonExit->setObjectName("ButtonSmall");

	this -> setStyleSheet(Style(":Small-Box")+Style(":Button-Small"));

	this -> show();
	this -> FadeIn();

	connect(ButtonExit, SIGNAL(clicked()), this, SLOT(FadeOut()));
}