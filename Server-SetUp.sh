#!/bin/bash

BLU='\033[0;34m'
CYA='\033[0;36m'
WHI='\033[1;37m'
YEL='\033[1;33m'
RED='\033[0;31m'
GRE='\033[0;32m'

BOL=$(tput bold)
NOR=$(tput sgr0)

printf " ${CYA} --------------------------------------------\n"
printf " ${WHI}       Sentinel Hub ${YEL}>${WHI}  Server Installation\n"
printf " ${CYA} ======================================================\n"

if [ "$EUID" -ne 0 ]
	then 
		printf "\n"
		printf "    ${RED}You need root priviledges to run this script.\n"
		printf "\n"
		printf " ${CYA} ======================================================\n"
		printf "  --------------------------------------------\n"
	exit
fi

printf "  ${BLU}::: ${YEL} Commands Output${BLU} :::::::::::::::::::::::::::::: ${WHI}\n\n" 


# Make Home directory
printf "    "
mkdir /home/Sentinel\ Hub/

# Make Client directory and add Version
printf "    "
mkdir /home/Sentinel\ Hub/Client
echo 1001 >> /home/Sentinel\ Hub/Client/Version

# Make Pk3 directory
printf "    "
mkdir /home/Sentinel\ Hub/Pk3

# Make a clean test asset
printf "    "
rm -rf /home/Sentinel\ Hub/Pk3/Test_Asset/*
printf "    "
mkdir /home/Sentinel\ Hub/Pk3/Test_Asset
printf "    "

# Give values to the test asset
echo 1 >> /home/Sentinel\ Hub/Pk3/Test_Asset/Version
echo "This file contains just random characters, for test purposes only" >> /home/Sentinel\ Hub/Pk3/Test_Asset/Description
echo "test" >> /home/Sentinel\ Hub/Pk3/Test_Asset/Owner

# Make a 16 Mb dummy file
cat /dev/urandom | env LC_CTYPE=C tr -dc 'a-zA-Z0-9' | fold -w 16777216 | head -n 1 > /home/Sentinel\ Hub/Pk3/Test_Asset/Asset.pk3


printf "\n"

printf "    "
groupadd hub-user
groupadd hub-admin
groupadd hub-dev
groupadd hub-server

printf "    "
useradd hub-test -g hub-user -d /home/Sentinel\ Hub

printf "    "
chown root:hub-dev -R /home/Sentinel\ Hub/
chown root:hub-user -R /home/Sentinel\ Hub/Pk3

chmod 771 -R /home/Sentinel\ Hub/Pk3 


printf "\n"

printf "    ${GRE}${BOL}Type the password for the 'hub-test' user ${NOR}\n"
printf "${WHI}        "
passwd hub-test
printf "    "

printf "\n"
printf "  ${BLU} :::::::::::::::::::::::::::::::::::::::::::::::::::\n" 

printf "\n"
printf "    ${YEL}Remember to add the Application.exe to the path : \n"
printf "         ${GRE}/home/Sentinel Hub/Client/Application \n"
printf "\n"

printf "\n"
printf "    ${YEL}Remember to allow the users from the group ${GRE}'hub-user' \n"
printf "    ${YEL}access to the command ${GRE}'passwd'"
printf "\n\n"

printf " ${CYA} ======================================================\n"
printf " ${WHI}           INSTALLATION COMPLETE${YEL}.\n"
printf " ${CYA} --------------------------------------------\n\n\n"
