
# Sentinel Hub
It is a C++ QT babsed program used to manage addons for the mod Movie Battles 2 mod from the
game Star Wars Jedi Knight: Jedi Academy.

### Features 
[x] Encrypted connection
[x] Authentication
[x] Enable / Disable addons
[x] Addon updates
[x] Multiple addon ownership
[x] User administration whithin application
[ ] Automatic application update